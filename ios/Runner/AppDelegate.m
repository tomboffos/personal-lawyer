#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import <OneSignal/OneSignal.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.
  [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"70631e06-e94f-4e8b-a74f-7588690a6546"
            handleNotificationAction:nil
                            settings:@{kOSSettingsKeyAutoPrompt: @false}];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
