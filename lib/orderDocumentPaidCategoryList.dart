import 'package:flutter/material.dart';
import 'package:personallawyer/CustomFunctios.dart';
import 'package:personallawyer/lang.dart';
import 'package:personallawyer/orderDocumentPaidCategoryPage.dart';
import 'package:personallawyer/orderDocumentPaidPage_step1.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import 'theme_settings.dart';


class orderDocumentPaidCategoryListScreen extends StatefulWidget{

  var id; var title;

  orderDocumentPaidCategoryListScreen({Key key, @required this.id, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentPaidCategoryListScreenState();
  }
}


class orderDocumentPaidCategoryListScreenState extends State<orderDocumentPaidCategoryListScreen>{

  String _api;
  var url;
  var categoryList;
  var categories;
  String _lang;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void getListCategory() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    if(_lang == 'rus') {
      url = "$Url/api/document/by-category/${widget.id}/ru/${_api}";
    } else {
      url = "$Url/api/document/by-category/${widget.id}/kk/${_api}";
    }

    print(url);

    final response = await http.get(url);

    categoryList = jsonDecode(response.body);

    print(categoryList);

    setState(() {
      categoryList;
      isLoading = false;
    });



    for (int i = 0; i < categoryList['data'].length; i++) {



      print(categoryList['data'][i]['id']);

        myList.add(
            new Column(
              children: <Widget>[
                ListTile(
                  title: Text(categoryList['data'][i]['doc_name']),
                  trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey,),
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => orderDocumentPaidCategoryPageScreen(
                              id: categoryList['data'][i]['id'],
                              doc_name: categoryList['data'][i]['doc_name'],
                              doc_description: categoryList['data'][i]['doc_description'],
                              doc_status: categoryList['data'][i]['doc_status'],
                              doc_category: categoryList['data'][i]['doc_category'],
                              doc_cost: categoryList['data'][i]['doc_cost'],
                              document_link: categoryList['data'][i]['document_link'],
                            )));
                  },
                ),
                Divider(height: 2,),
              ],
            )
        );

    }

  }


  void initState() {
    getListCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          flexibleSpace: Container(
          decoration: new BoxDecoration(
            gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Color(0xff4267b2),
                  Color(0xff4267b2),
                ],
                tileMode: TileMode.repeated),
          ),
        ),
        ),
        body:  isLoading
            ? buildInit()
            :categoryList==null||categoryList['data'].length==0?buildEmpty(trans('no_data',_lang)): SingleChildScrollView(
          child: Container(
            child: Column(
              children: myList,
            ),
          ),
        )

    );
  }

}