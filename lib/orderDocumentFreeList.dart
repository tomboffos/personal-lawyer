import 'package:flutter/material.dart';
import 'package:personallawyer/orderDocumentFreePage.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'theme_settings.dart';


class orderDocumentFreeListScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentFreeListScreenState();
  }
}


class orderDocumentFreeListScreenState extends State<orderDocumentFreeListScreen>{

  String _api;
  var url;
  var documents;

  List<Widget>myList = new List();
  ListView list;

  void getList() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "$Url/api/$_api/document/free_document";

    print(url);

    final response = await http.get(url);

    documents = jsonDecode(response.body);

    print(documents);

    setState(() {
      documents;
    });

    for (int i = 0; i < documents.length; i++) {
      myList.add(
          new Column(
            children: <Widget>[
              ListTile(
                title: Text(documents[i]['doc_name']),
                onTap: (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => orderDocumentFreePageScreen(id: documents[i]['id'], title: documents[i]['doc_name'])));
                },
              ),
              Divider(height: 2,),
            ],
          )

      );
    }

  }

  void initState() {
    getList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
              child: Column(
                children: myList,
              ),
          ),
        )

    );
  }

}