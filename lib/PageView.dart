import 'package:flutter/material.dart';
import 'package:personallawyer/AskQuestion_step1.dart';
import 'package:personallawyer/orderDocument.dart';
import 'CustomFunctios.dart';
import 'mainMenuBody.dart';
import 'navDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:share/share.dart';
import 'lang.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'theme_settings.dart';

class PagerScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PagerScreenState();
  }
}


class PagerScreenState extends State<PagerScreen> with SingleTickerProviderStateMixin{

  TabController tabC;
  var url;
  var data;
  var user;
  String _api;
  String _user_image = "";
  String _number;
  String _lang;
  bool isLoading = true;
  var PriceData;
  int count;

  void getCount() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/answers/count";
    final response = await http.get(url);
    setState(() {
      count = jsonDecode(response.body)['count'];
    });

  }
  void checkNumber() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "$Url/api/$_api/phone_feedback/";

    final response = await http.get(url);

    data = jsonDecode(response.body);

    sp.setString('phone_number', data);

//    setState(() {
//
//      data;
//
//    });

  }

  void getInfo() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _number = sp.getString("phone_number");
  }

  void checkUser() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

   // print(_api);

    url = "$Url/api/$_api/about_you/user/user_data";

  //  print(url);

    final response = await http.get(url);

    user = jsonDecode(response.body);

   // print('user $user');

    sp.setString('user_image', user["image_link"]);
    sp.setString('phone_number_user', user["phone_number"]);
    sp.setString('nameText', user['user_name']);
    sp.setString('emailText', user['user_email']);
   // sp.setString('cityID', user['user_city']);
    sp.setInt('user_id', user['id']);

  }

  void test() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
   // print(sp.getKeys());
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void checkPrice() async {

    url = "$Url/api/get-cost/consultation";

    final response = await http.post(url, body: {'type': 'all'});

    if (response.statusCode == 200) {

      PriceData = jsonDecode(response.body);

      print(PriceData);

      setState(() {
        PriceData;
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load files');
    }
  }

  void initState() {
    checkPrice();
    checkUser();
    test();
    getLang();
    getCount();
    super.initState();

  }

  @override
  void dispose(){
    super.dispose();
  }
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;

    queryData = MediaQuery.of(context);

    final w = queryData.size.width;
    final h = queryData.size.height;
    getLang();
    return Scaffold(
        endDrawerEnableOpenDragGesture: false, // THIS WAY IT WILL NOT OPEN

        drawer: NavDrawer(count:count),
        key: _drawerKey, // assign key to Scaffold

      appBar: AppBar(


        leading:  Stack(

          children: [
            Positioned(
              top: 20,
              left: 10,
              child: GestureDetector(
                onTap: (){
                  _drawerKey.currentState.openDrawer();
                  },
                child: Container(
                  child: Icon(
                      Icons.menu
                  ),
                ),
              ),
            ),

            Positioned(
              left: 25,
              top: 10,
              child: Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Center(
                  child: Text('$count',style: TextStyle(color:Colors.white),),
                ),
              ),

            ),
          ],

        ),

        backgroundColor: Colors.transparent,
        elevation: 0.0,


        flexibleSpace: Container(
          decoration: new BoxDecoration(
            gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Color(0xff4267b2),
                  Color(0xff4267b2),
                ],
                tileMode: TileMode.repeated),
          ),
        ),


        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            AutoSizeText("Ваш личный адвокат",
                style: TextStyle(color: Colors.white),
              maxFontSize: 16,
              minFontSize: 9,
            ),
            AutoSizeText("www.personal-lawyer.kz",
              style: TextStyle(color: Colors.white),
              maxFontSize: 13,
              minFontSize: 9,
            ),
          ],
        ),
        //iconTheme: new IconThemeData(color: Colors.black),
        actions: <Widget>[

          FlatButton(
            child: Icon(Icons.share,
                color: Colors.white),
            onPressed: (){
              shareApp();
            },
          ),

        ],
      ),

//      body: MainMenuBody(),

    // extendBody: false,

      body: GestureDetector(
        onTap: (){
          getCount();
        },
        child: Center(
          child:  Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/fon_main03.png"),
                fit: BoxFit.cover,
              ),
            ),
            child:   isLoading
                ? Center(
                child: CircularProgressIndicator())
                : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[


                        InkWell(
                          child: Container(
                            margin: EdgeInsets.only(top: 0),
                            width: MediaQuery.of(context).size.width / 1.1,
                            padding: EdgeInsets.all(8) ,
                            decoration: BoxDecoration(
                              color: Color(0xff4267b2),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              children: <Widget>[
                                Text(trans('request_a_consultation', _lang), textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16),),
                                Container(
                                  padding: EdgeInsets.only(top: 2.5),
                                ),
                                Text('${PriceData[1]['cost']} тенге', textAlign: TextAlign.center, style: TextStyle(color: Colors.white,),),
                              ],
                            )
                          ),
                          onTap: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AskQuestion_Step1Screen()));
                          },
                        ),

                        InkWell(
                          child: Container(
                            margin: EdgeInsets.only(top: 10),
                            width: MediaQuery.of(context).size.width / 1.1,
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: Color(0xff4267b2),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              children: <Widget>[
                                Text(trans('order_documents', _lang), textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16),),
                                Container(
                                  padding: EdgeInsets.only(top: 2.5),
                                ),
                                Text('${PriceData[2]['cost']} тенге', textAlign: TextAlign.center, style: TextStyle(color: Colors.white,),),
                              ],
                            )
                          ),
                          onTap: () async {
                            print('hello');
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => orderDocumentScreen(title: trans('order_documents', _lang))));
                          },
                        ),



                      ],
                    )
            ),
                  ],
                ),
          ),
        ),
      )








//      bottomNavigationBar: Container(
//          margin: EdgeInsets.only(right: 5, left: 5, bottom: 30),
//          alignment: Alignment.bottomCenter,
//
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Container(
//                decoration: BoxDecoration(
//                  color: Color(0xff4267b2),
//                  borderRadius: BorderRadius.circular(5),
//                ),
//                width: 200,
//                height: 50,
//                padding: EdgeInsets.only(left: 10, right: 10,),
//                child:  Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    Text(trans('request_a_consultation', _lang), style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
//                  //  Text('3000 тенге', style: TextStyle(fontSize: 12, color: Colors.white),)
//                  ],
//                )
//              ),
//
//              Container(
//                decoration: BoxDecoration(
//                  color: Color(0xff4267b2),
//                  borderRadius: BorderRadius.circular(5),
//                ),
//                width: 200,
//                height: 50,
//                padding: EdgeInsets.only(left: 10, right: 10,),
//                child:  Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    Text(trans('order_documents', _lang), style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
//                  //  Text('3000 тенге', style: TextStyle(fontSize: 12, color: Colors.white),)
//                  ],
//                )
//              ),
//
//            ],
//          )
//      ),

    );
  }




}