import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:expandable/expandable.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:personallawyer/reviewsPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';
import 'theme_settings.dart';

class UserProfilScreen extends StatefulWidget {
  var id;
  var questionid;

  UserProfilScreen({Key key, @required this.id, this.questionid}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return UserProfilScreenState();
  }
}

class UserProfilScreenState extends State<UserProfilScreen> {
  String _api;
  String _lang;
  var data;
  var url;
  var temp;
  var isLoading = true;
  var edu;
  var dataEducation;
  List<Widget> myList = new List();
  List<Widget> mycoursList = new List();
  List<Widget> reviewsList = new List();
  ListView list;
  var cours;
  var reviews;
  var reviews_user;
  var inactiveButtons = false;
  var _userPositive;

  var _userNegative;

  void userProfile() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "$Url/api/$_api/profile/${widget.id}/${widget.questionid}/$_lang/";

   // print(url);

    final response = await http.get(url);

    if (response.statusCode == 200) {
      data = jsonDecode(response.body);

     // print('data $data');

   //   print(data['gol']);

      inactiveButtons = data['gol'];
      // edu = new List.from(data["education"]);
      // cours = new List.from(data["courses"]);
      //reviews = new List.from(data["reviews"]);

      setState(() {
        data;
        reviews;
        _userPositive = data['positive_rating'].toString();
        _userNegative = data['negative_rating'].toString();
        data["education"] == null ?? eduLists();
        data["courses"] == null ?? coursLists();

        if (data['reviews'] != null) {
          reviewsLits();
        }

        isLoading = false;
      });
    } else {
      throw Exception('Failed to load files');
    }
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void eduLists() {
    edu = new List.from(data["education"]);

    for (int i = 0; i < edu.length; i++) {
      myList.add(new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 5),
            child: Text(
              edu[i]['header_education'] == null
                  ? ''
                  : edu[i]['header_education'],
              style: TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.left,
            ),
          ),
          Container(
            child: Text(
              edu[i]['list_education'] == null ? '' : edu[i]['list_education'],
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ));
    }
  }

  void coursLists() {
    cours = new List.from(data["courses"]);
    for (int i = 0; i < cours.length; i++) {
      mycoursList.add(new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 5),
            child: Text(
              cours[i]['header_courses'] == null
                  ? ''
                  : cours[i]['header_courses'],
              style: TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.left,
            ),
          ),
          Container(
            child: Text(
              cours[i]['list_courses'] == null ? '' : cours[i]['list_courses'],
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ));
    }
  }

  void reviewsLits() {
    reviews = new List.from(data["reviews"]);

    for (int i = 0; i < reviews.length; i++) {
      reviews_user = new Map.from(reviews[i]["reviews_user"]);

      reviewsList.add(new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              //color: Colors.red,
              padding: EdgeInsets.only(top: 15.0),
              child: Row(
                children: <Widget>[
                  CircularProfileAvatar(
                    '$Url' + reviews[i]['image_link'],
                    radius: 35,
                    backgroundColor: Colors.transparent,
                    //borderWidth: 3,
                    //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
                    borderColor: Colors.white,
                    elevation: 5.0,
                    //foregroundColor: Colors.white10.withOpacity(0.5),
                    cacheImage: true,
                    onTap: () {
                      // getImage(context);
                    },
                    showInitialTextAbovePicture: true,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          child: Container(
                            child: Text(reviews[i]["user_name"] == null
                                ? ''
                                : reviews[i]["user_name"]),
                          ),
                          onTap: () {},
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Text(
                            'Пользователь',
                            style: TextStyle(color: Colors.grey),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              )),
          Container(
            padding: EdgeInsets.only(top: 15),
            child: Divider(
              height: 2,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            //child: Text(reviews[i]["reviews_user"][i]["reviews"]),
            child: Text(
                reviews_user["reviews"] == null ? '' : reviews_user["reviews"]),
          ),
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                reviews_user["created"] == null ? '' : reviews_user["created"],
                textAlign: TextAlign.left,
                style: TextStyle(color: Colors.grey, fontSize: 14),
              ),
            ),
          ),
          Divider(
            height: 2.0,
          ),
        ],
      ));
    }
  }

  void toRate(var status) async {
    setState(() {
      inactiveButtons = true;
    });
    var response = await http.post("$Url/api/profile/assessment", body: {
      'api_token': _api,
      'user_id': widget.id.toString(),
      'point': status.toString(),
      'question': widget.questionid
    });
    var data = jsonDecode(response.body);
    print(data);
    setState(() {
      _userPositive = data[0];
      _userNegative = data[1];
    });
  }

  void initState() {
    userProfile();
   // print(reviews);
    super.initState();
    getLang();
  }

  bool disableButton = false;

  @override
  Widget build(BuildContext context) {
    getLang();

   // !isLoading ? print('avatar api: ${'$Url' + data["image_link"]}') : true;

    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          title: Text(trans('lawyer_profile', _lang),
              style: TextStyle(color: Colors.white)),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Container(
                // padding: EdgeInsets.only(top: 10.0,),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        //color: Colors.red,
                        padding: EdgeInsets.only(left: 20, top: 15.0),
                        child: Row(
                          children: <Widget>[
                            CircularProfileAvatar(
                              '$Url' + data["image_link"],
                              radius: 50,
                              backgroundColor: Colors.transparent,
                              //borderWidth: 3,
                              //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
                              borderColor: Colors.white,
                              elevation: 5.0,
                              //foregroundColor: Colors.white10.withOpacity(0.5),
                              cacheImage: true,
                              onTap: () {
                                // getImage(context);
                              },
                              showInitialTextAbovePicture: true,
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      data["name"] == null ? '' : data["name"],
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 5),
                                    child: Text(
                                      data["roles_id"] + ", " + data["city"],
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 1),
                                    child: Text(
                                      trans('reviews', _lang) +
                                          data["number_of_reviews"].toString(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 1),
                                    child: Text(
                                      trans('positive_evaluation', _lang) +
                                          _userPositive.toString(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )),
                    Container(
                      padding: EdgeInsets.only(
                          top: 20, left: 20, right: 20, bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(right: 20),
                                  child: Row(
                                    children: <Widget>[
//                                    Container(
//                                        width: 25,
//                                        height: 25,
//                                        decoration: BoxDecoration(
//                                            color: Colors.green,
//                                            borderRadius: BorderRadius.circular(50)
//                                        ),
//                                        child: Align(
//                                          alignment: Alignment.topCenter,
//                                          child: Text('+', style: TextStyle(color: Colors.white, fontSize: 18),),
//                                        )
//                                    ),
                                      inactiveButtons == true
                                          ? Container(
                                        width: 25,
                                        height: 25,
                                        child: Icon(Icons.thumb_up,
                                            color: Colors.grey),
                                      )
                                          : InkWell(
                                        onTap: () => toRate(1),
                                        child: Container(
                                        width: 25,
                                        height: 25,
                                        child: Container(
                                            width: 25,
                                            height: 25,
                                          child: Icon(Icons.thumb_up, color: Colors.green,)
                                        ),
                                      ),),

                                      Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                          _userPositive.toString(),
                                          style: TextStyle(
                                              color: Colors.green,
                                              fontSize: 16),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(right: 20),
                                  child: Row(
                                    children: <Widget>[

                                      inactiveButtons == true
                                          ? Container(
                                              width: 25,
                                              height: 25,
                                              child: Icon(Icons.thumb_down,
                                                  color: Colors.grey),
                                            )
                                          : Container(
                                              width: 25,
                                              height: 25,
                                              child: GestureDetector(
                                                  onTap: () => toRate(0),
                                                  child: Icon(Icons.thumb_down,
                                                      color: Colors.red)),
                                            ),

                                      Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                          _userNegative.toString(),
                                          style: TextStyle(
                                              color: Colors.red, fontSize: 16),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          InkWell(
                            child: Container(
                              child: Text(
                                trans('write_a_review', _lang),
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ReviewsPageScreen(
                                        id: data['id'].toString())),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                    Divider(
                      height: 2,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 15, left: 20, right: 20, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Text(
                              trans('education', _lang),
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children:
                                data["education"] == null ? <Widget>[] : myList,
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 2,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 15, left: 20, right: 20, bottom: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Text(
                              trans('courses', _lang),
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: data["courses"] == null
                                ? <Widget>[]
                                : mycoursList,
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 2,
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 15, left: 20, right: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Text(
                              trans('reviews2', _lang),
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                data['reviews'] == null
                                    ? Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Center(
                                          child:
                                              Text(trans('no_reviews', _lang)),
                                        ))
                                    : Column(
                                        children: reviewsList,
                                      )

//                              Container(
//                                child: Column(
//                                  children: <Widget>[
//
//                                    Container(
//                                      //color: Colors.red,
//                                        padding: EdgeInsets.only( top:15.0),
//                                        child:  Row(
//                                          children: <Widget>[
//                                            CircularProfileAvatar(
//                                              'http://database.itgk.kz/img/abramova.png',
//                                              radius: 35,
//                                              backgroundColor: Colors.transparent,
//                                              //borderWidth: 3,
//                                              //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
//                                              borderColor: Colors.white,
//                                              elevation: 5.0,
//                                              //foregroundColor: Colors.white10.withOpacity(0.5),
//                                              cacheImage: true,
//                                              onTap: () {
//                                                // getImage(context);
//                                              },
//                                              showInitialTextAbovePicture: true,
//                                            ),
//                                            Container(
//                                              padding: EdgeInsets.only(left: 15),
//                                              child: Column(
//                                                crossAxisAlignment: CrossAxisAlignment.start,
//                                                children: <Widget>[
//                                                  InkWell(
//                                                    child: Container(
//                                                      child: Text('Абрамова Юлия'),
//                                                    ),
//                                                    onTap: (){
//
//                                                    },
//                                                  ),
//                                                  Container(
//                                                    padding: EdgeInsets.only(top: 5),
//                                                    child: Text('Пользователь', style: TextStyle(color: Colors.grey),),
//                                                  )
//                                                ],
//                                              ),
//                                            )
//                                          ],
//                                        )
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 15),
//                                      child: Divider(height: 2,),
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 10),
//                                      child: Text('Выражаю Ротову Алексею глубокую признательность за проведенную работ, быстрое достижение поставленной цели и комфортное сотрудничество. С радостью обращусь снова, если возникнет такая потребность, и буду рекомендовать всем партнерам.'),
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 10, bottom: 10),
//                                      child: Align(
//                                        alignment: Alignment.centerLeft,
//                                        child: Text('Вчера, 18:15', textAlign: TextAlign.left, style: TextStyle(color: Colors.grey, fontSize: 14),),
//                                      ),
//                                    ),
//
//                                    Divider(height: 2.0,),
//
//                                  ],
//                                ),
//                              ),
//
//                              Container(
//                                child: Column(
//                                  children: <Widget>[
//
//                                    Container(
//                                      //color: Colors.red,
//                                        padding: EdgeInsets.only( top:15.0),
//                                        child:  Row(
//                                          children: <Widget>[
//                                            CircularProfileAvatar(
//                                              'http://database.itgk.kz/img/abramova.png',
//                                              radius: 35,
//                                              backgroundColor: Colors.transparent,
//                                              //borderWidth: 3,
//                                              //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
//                                              borderColor: Colors.white,
//                                              elevation: 5.0,
//                                              //foregroundColor: Colors.white10.withOpacity(0.5),
//                                              cacheImage: true,
//                                              onTap: () {
//                                                // getImage(context);
//                                              },
//                                              showInitialTextAbovePicture: true,
//                                            ),
//                                            Container(
//                                              padding: EdgeInsets.only(left: 15),
//                                              child: Column(
//                                                crossAxisAlignment: CrossAxisAlignment.start,
//                                                children: <Widget>[
//                                                  InkWell(
//                                                    child: Container(
//                                                      child: Text('Абрамова Юлия'),
//                                                    ),
//                                                    onTap: (){
//
//                                                    },
//                                                  ),
//                                                  Container(
//                                                    padding: EdgeInsets.only(top: 5),
//                                                    child: Text('Пользователь', style: TextStyle(color: Colors.grey),),
//                                                  )
//                                                ],
//                                              ),
//                                            )
//                                          ],
//                                        )
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 15),
//                                      child: Divider(height: 2,),
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 10),
//                                      child: Text('Выражаю Ротову Алексею глубокую признательность за проведенную работ, быстрое достижение поставленной цели и комфортное сотрудничество. С радостью обращусь снова, если возникнет такая потребность, и буду рекомендовать всем партнерам.'),
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 10, bottom: 10),
//                                      child: Align(
//                                        alignment: Alignment.centerLeft,
//                                        child: Text('Вчера, 18:15', textAlign: TextAlign.left, style: TextStyle(color: Colors.grey, fontSize: 14),),
//                                      ),
//                                    ),
//
//                                    Divider(height: 2.0,),
//
//                                  ],
//                                ),
//                              ),
//
//                              Container(
//                                child: Column(
//                                  children: <Widget>[
//
//                                    Container(
//                                      //color: Colors.red,
//                                        padding: EdgeInsets.only( top:15.0),
//                                        child:  Row(
//                                          children: <Widget>[
//                                            CircularProfileAvatar(
//                                              'http://database.itgk.kz/img/abramova.png',
//                                              radius: 35,
//                                              backgroundColor: Colors.transparent,
//                                              //borderWidth: 3,
//                                              //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
//                                              borderColor: Colors.white,
//                                              elevation: 5.0,
//                                              //foregroundColor: Colors.white10.withOpacity(0.5),
//                                              cacheImage: true,
//                                              onTap: () {
//                                                // getImage(context);
//                                              },
//                                              showInitialTextAbovePicture: true,
//                                            ),
//                                            Container(
//                                              padding: EdgeInsets.only(left: 15),
//                                              child: Column(
//                                                crossAxisAlignment: CrossAxisAlignment.start,
//                                                children: <Widget>[
//                                                  InkWell(
//                                                    child: Container(
//                                                      child: Text('Абрамова Юлия'),
//                                                    ),
//                                                    onTap: (){
//
//                                                    },
//                                                  ),
//                                                  Container(
//                                                    padding: EdgeInsets.only(top: 5),
//                                                    child: Text('Пользователь', style: TextStyle(color: Colors.grey),),
//                                                  )
//                                                ],
//                                              ),
//                                            )
//                                          ],
//                                        )
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 15),
//                                      child: Divider(height: 2,),
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 10),
//                                      child: Text('Выражаю Ротову Алексею глубокую признательность за проведенную работ, быстрое достижение поставленной цели и комфортное сотрудничество. С радостью обращусь снова, если возникнет такая потребность, и буду рекомендовать всем партнерам.'),
//                                    ),
//
//                                    Container(
//                                      padding: EdgeInsets.only(top: 10, bottom: 10),
//                                      child: Align(
//                                        alignment: Alignment.centerLeft,
//                                        child: Text('Вчера, 18:15', textAlign: TextAlign.left, style: TextStyle(color: Colors.grey, fontSize: 14),),
//                                      ),
//                                    ),
//
//                                    Divider(height: 2.0,),
//
//                                  ],
//                                ),
//                              ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )));
  }
}
