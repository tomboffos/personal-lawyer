import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'lang.dart';
import 'AskQuestion_step2.dart';
import 'AskQuestion_step3.dart';
import 'theme_settings.dart';

void AskQuestionCity() {
  runApp(MaterialApp(
    home: AskQuestion_Step2CityScreen(),
    debugShowCheckedModeBanner: false,
  ));
}

class AskQuestion_Step2CityScreen extends StatefulWidget {
  @override
  AskQuestion_Step2CityScreenState createState() => AskQuestion_Step2CityScreenState();
}

class AskQuestion_Step2CityScreenState extends State<AskQuestion_Step2CityScreen> {

  String _api;
  var url;
  var city;
  var cityText;
  var cityID;
  String _lang;
  var loading = false;

  List<Posts> _list = [];
  List<Posts> _search = [];

  void setCity() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('cityText', cityText);
    sp.setString('cityID', cityID);
    print('selected city id $cityID');
  }

  Future<Null> fetchData() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    setState(() {
      loading = true;
    });
    _list.clear();
    print('$Url/api/$_api/cities/$_lang/');
    final response =
    await http.get('$Url/api/$_api/cities/$_lang/');
    print(response.body);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(Posts.formJson(i));
          loading = false;
        }
      });
    }
  }

  TextEditingController controller = new TextEditingController();

  onSearch(String text) async {
    _search.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _list.forEach((f) {
      if (f.cities.contains(text))
        _search.add(f);
    });

    setState(() {});
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    super.initState();
    fetchData();
    getLang();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('choose_a_city', _lang), style: TextStyle(color: Colors.white)),

          leading: Text(''),

          actions: <Widget>[
            FlatButton(
                child: Container(
                  child: Text(trans('step2of3', _lang), style: TextStyle(color: Colors.white),),
                )
            )
          ],
        ),

        body: Container (
              child: Column(

                children: <Widget>[

                  Container(
                    padding: EdgeInsets.all(5.0),
                    color: Color(0xffcac9cf),
                    child: Card(
                      child: ListTile(
                        leading: Icon(Icons.search, size: 35,),
                        title: TextField(
                          textCapitalization: TextCapitalization.sentences,
                          controller: controller,
                          onChanged: onSearch,
                          decoration: InputDecoration(
                              hintText: trans('search', _lang),
                              border: InputBorder.none,
                          ),
                        ),
                        trailing: IconButton(
                          onPressed: () {
                            controller.clear();
                            onSearch('');
                          },
                          icon: Icon(Icons.cancel),
                        ),
                      ),
                    ),
                  ),

                  Container(
                    child: loading
                        ? Center(
                      child: CircularProgressIndicator(),
                    )
                        : Expanded(
                      child: _search.length != 0 || controller.text.isNotEmpty
                          ? ListView.builder(
                        itemCount: _search.length,
                        itemBuilder: (context, i) {
                          final b = _search[i];
                          return Container(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,

                                children: <Widget>[

                                  ListTile(
                                    title: Text(b.cities),
                                    onTap: (){
                                      setCity();
                                      cityText = b.cities;
                                      cityID = b.id;

                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(
                                              builder: (context) => AskQuestion_Step2Screen()));

//                                      Navigator.push(
//                                          context,
//                                          MaterialPageRoute(
//                                              builder: (context) => AskQuestion_Step2Screen()));
                                    },
                                  ),

                                  Divider(height:2),

                                ],
                              ));
                        },
                      )
                          : ListView.builder(
                        itemCount: _list.length,
                        itemBuilder: (context, i) {
                          final a = _list[i];
                          return Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                  ListTile(
                                    title: Text(a.cities),
                                    onTap: (){
                                      setCity();
                                      cityText = a.cities;
                                      cityID = a.id;

                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(
                                               builder: (context) => AskQuestion_Step2Screen()));

//                                      Navigator.push(
//                                            context,
//                                            MaterialPageRoute(
//                                                builder: (context) => AskQuestion_Step2Screen()));
                                    },
                                  ),

                                  Divider(height:2),

                                ],
                              ));
                        },
                      ),
                    ),
                  )


                ],
              ),
            )
    );
  }
}

class Posts {
  final String cities;
  final String id;

  Posts({this.cities, this.id});

  factory Posts.formJson(Map <String, dynamic> json){
    return new Posts(
      cities: json['cities'],
      id: json['id'].toString()
    );
  }

}