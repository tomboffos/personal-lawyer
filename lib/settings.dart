import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';
import 'feedbackTopic.dart';
import 'theme_settings.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SettingsScreenState();
  }
}

class SettingsScreenState extends State<SettingsScreen> {

  String _api;
  var url;
  bool _notification = false;
  var status;
  var isLoading = true;

  void Notification() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "$Url/api/$_api/settings/status_notify_check";

    final response = await http.get(url);

    print(_api);

    print(response.body);

    status = jsonDecode(response.body);

    print(status);



    if(status == 'notify') {

      _notification = true;

    } else if(status == "do_not_notify"){

      _notification = false;


    }

    setState(() {
      _notification;

      isLoading = false;
    });

  }

void notify(bool change) async {
    var _status;
    if(change) {
      _status = 'notify';
    } else {
      _status = 'do_not_notify';
    }
    setState(() {
      _notification = change;
    });
    url = "$Url/api/$_api/settings/status_notify/$_status";
    final response = await http.get(url);
    if(response.statusCode == 200)
      print(response.body);
    else
      print('Error');
}

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    Notification();
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('settings', _lang), style: TextStyle(color: Colors.white)),
        ),

        body: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            :  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 12.0, right: 12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[

                        ListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(trans('notification', _lang)),

                              Switch(
                                  value: _notification,
                                onChanged: (val) {
                                  notify(val);
                                },
                              )

//                              GFToggle(
//                                disabledTrackColor: Colors.blueGrey,
//                                onChanged: (val){
//                                  if(_notification == false){
//                                    setState(() {
//                                      _notification = true;
//                                      print('NET');
//                                    });
//                                  } else{
//                                    setState(() {
//                                      _notification = false;
//                                      print('DA');
//                                    });
//                                  }
//                                },
//                                value: null,
//                                type: GFToggleType.ios,
//                              ),
                            ],
                          ),
                        ),

                        Divider(height: 2.0,),

//                        ListTile(
//                          title: Text(trans('feedback2', _lang)),
//                          trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey),
//                          onTap: () {
//                            Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                    builder: (context) => FeedbackTopicScreen()));
//                          },
//                        ),
//                        Divider(height: 2.0,),
                        



                      ],
                    ),
                  ),


                  

                ],
              ),
            )
        )
    );
  }
}