import 'package:flutter/material.dart';
import 'package:personallawyer/orderDocumentPaidPage_step1.dart';
import 'package:personallawyer/paybox/url_launcher_document.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:share/share.dart';

import 'theme_settings.dart';

class orderDocumentPaidCategoryPagePayScreen extends StatefulWidget{

  var id; var doc_name; var doc_description; var doc_category; var doc_status; var doc_cost;
  var document_link;

  orderDocumentPaidCategoryPagePayScreen({Key key, @required this.id, this.doc_name, this.doc_category, this.doc_cost, this.doc_description, this.doc_status, this.document_link}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentPaidCategoryPagePayScreenState();
  }
}


class orderDocumentPaidCategoryPagePayScreenState extends State<orderDocumentPaidCategoryPagePayScreen>{

  String _api;
  var url;
  var data;
  var categories;
  String _lang;
  var isLoading = false;
  List<Widget>myList = new List();
  ListView list;

  void getLinkPay() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "$Url/api/document/get-payment-url/${widget.id}/${_api}";

    print(url);

    final response = await http.get(url);

    data = jsonDecode(response.body);

    print(data);

    setState(() {
      data;


    });

  }


  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Документ №: ' + widget.id.toString()),
        flexibleSpace: Container(
          decoration: new BoxDecoration(
            gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Color(0xff4267b2),
                  Color(0xff4267b2),
                ],
                tileMode: TileMode.repeated),
          ),
        ),
      ),
      body:  isLoading
          ? Center(
          child: CircularProgressIndicator())
          : SingleChildScrollView(
          child: Container (
            padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
            child: Column(
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(widget.doc_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Row(
                          children: <Widget>[
                            Text('Тип документ:', style: TextStyle(color: Colors.grey, fontSize: 14),),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text(widget.doc_status, style: TextStyle(fontSize: 14),),
                            )
                          ],
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Row(
                          children: <Widget>[
                            Text('Цена:', style: TextStyle(color: Colors.grey, fontSize: 14),),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text(widget.doc_cost + ' тенге', style: TextStyle(fontSize: 14),),
                            )
                          ],
                        ),
                      ),

                      widget.doc_description == 'null' || widget.doc_description == null || widget.doc_description == '' ? Container() :

                      Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Описание:', style: TextStyle(color: Colors.grey, fontSize: 14),),
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              child: Text(widget.doc_description, style: TextStyle(fontSize: 14),),
                            )
                          ],
                        ),
                      ),

                    ],
                  ),
                ),


              ],
            ),
          )
      ),

      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: () {

            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.payment,
                    color: Colors.white
                ),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text('Сформировать счет',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                ),


              ],
            )
        ),
      ),

    );
  }

}