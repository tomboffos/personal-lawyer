import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:personallawyer/AskQuestion_step1.dart';
import 'package:personallawyer/QuestionPage.dart';
import 'package:personallawyer/lang.dart';
import 'navDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'orderDocument.dart';
import 'theme_settings.dart';


class MainMenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(

        ),
        drawer: NavDrawer(),
        body: MainMenuBody()
    );
  }
}

class MainMenuBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainMenuBodyState();
  }
}

class MainMenuBodyState extends State<MainMenuBody> {

  String _api;
  String _lang;
  var url;
  List<Posts> _listMap = [];
  var loading = false;
  var isLoading = true;

  ScrollController _scrollController;
  var postsCount = 0;
  var contentIsDone = false;

  Future<Null> fetchData() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

      if (mounted) {
        setState(() {
          loading = true;
        });
      }

    url = "$Url/api/$_api/question/questions_on_the_project/$postsCount/$_lang";

    final response = await http.get(url);

    print(url);
    print(url);
    print(url);
    print(url);
    print(url);
    print(url);

    //print(response.statusCode);
    if (response.statusCode == 200) {

      var data = jsonDecode(response.body);

      print(data);

      if (mounted && data != null) {
        setState(() {
          for (Map i in data) {
            _listMap.add(Posts.formJson(i));
            loading = false;

            isLoading = false;
          }
        });
      }
    } else {
      contentIsDone = true;
    }
  }

  @override
  void initState(){
    _scrollController = new ScrollController()..addListener(_scrollListener);
    fetchData();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

 return Scaffold(
   body: isLoading
       ? Center(
     child: CircularProgressIndicator(),
   )
       : ListView.builder(
       controller: _scrollController,
       itemCount: _listMap.length,
       itemBuilder: (context, i) {
         final a = _listMap[i];
         return Column(
           children: <Widget>[

             ListTile(
               title: Container(
                 padding: EdgeInsets.only(top: 5, bottom: 5),
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
//                     a.issue_price == 'null' ? Container() :
//                     Container(
//                       padding: EdgeInsets.only(top: 5, bottom: 5),
//                       child: Text('Цена вопроса: ' + a.issue_price, style: TextStyle(fontSize: 15, color: Colors.green),),
//                     ),
                     Text(a.header_question, style: TextStyle(fontWeight: FontWeight.bold),),
                     Container(
                       padding: EdgeInsets.only(top: 5, bottom: 5),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: <Widget>[
//                           Container(
//                             child: Row(
//                               children: <Widget>[
//                                 Icon(Icons.chat_bubble, color: Colors.grey, size: 20,),
//                                 a.new_answers_count == 'null' ?
//                                 Container(
//                                   margin: EdgeInsets.only(left: 5),
//                                   child: Text('0 ответов', style: TextStyle(fontSize: 14),),
//                                 )
//                                     :
//                                 Text(a.new_answers_count + ' ответов', style: TextStyle(fontSize: 14),),
//                               ],
//                             ),
//                           ),
                           Container(
                             child: Text(a.created, style: TextStyle(color: Colors.grey, fontSize: 14),),
                           )
                         ],
                       ),
                     )
                   ],
                 ),
               ),
               onTap:() {
                 Navigator.push(
                     context,
                     MaterialPageRoute(
                         builder: (context) => QuestionPageScreen(id_question: a.id,)));
               },
               trailing: Icon(Icons.arrow_forward_ios),
             ),

             Divider(
               height: 0.2,
             ),

             i == _listMap.length - 1 ?
             contentIsDone
                 ?
             Container()
                 :
             Container(
               margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
               alignment: Alignment.bottomCenter,
               child: CircularProgressIndicator(),
             )
                 :
             Container()

           ],
         );
       }),
   bottomNavigationBar: Container(
       height: 55,
       decoration: BoxDecoration(
         border: Border(
           top: BorderSide(width: 1.0, color: Colors.grey[300]),
         ),
         color: Color(0xff4267b2).withOpacity(0.2),
       ),
       margin: EdgeInsets.only(bottom: 5),
       padding: EdgeInsets.only(right: 5, left: 5,),
       alignment: Alignment.bottomCenter,
       child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: <Widget>[

           InkWell(
             child: Container(
                 decoration: BoxDecoration(
                   color: Color(0xff4267b2),
                   borderRadius: BorderRadius.circular(5),
                 ),
                 width: MediaQuery.of(context).size.width / 2 - 7.5,
                 height: 50,
                 padding: EdgeInsets.only(left: 10, right: 10,),
                 child:  Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     Text(trans('request_a_consultation', _lang), style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
                       Text('3000 тенге', style: TextStyle(fontSize: 12, color: Colors.white),)
                   ],
                 )
             ),
             onTap: (){
               Navigator.push(context, MaterialPageRoute(
                   builder: (context) => AskQuestion_Step1Screen()));
             },
           ),

           InkWell(
             child: Container(
                 decoration: BoxDecoration(
                   color: Color(0xff4267b2),
                   borderRadius: BorderRadius.circular(5),
                 ),
                 width: MediaQuery.of(context).size.width / 2 - 7.5,
                 height: 50,
                 padding: EdgeInsets.only(left: 10, right: 10,),
                 child:  Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     Text(trans('order_documents', _lang), style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
                       Text('3000 тенге', style: TextStyle(fontSize: 12, color: Colors.white),)
                   ],
                 )
             ),
             onTap: (){
               Navigator.push(context, MaterialPageRoute(
                   builder: (context) => orderDocumentScreen(title: trans('order_documents', _lang))));
             },
           )

         ],
       )
   ),
 );


  }

  void _scrollListener() {

    if(_scrollController.position.extentAfter < 100 && contentIsDone == false){
      print(_scrollController.position.extentAfter);

      setState(() {
        isLoading = false;
        fetchData();
          postsCount += 20;
       // await
      });
    }
  }
}

class Posts {
  final String id;
  final String header_question;
  final String city_question;
  final String text_question;
  final String mobile_users_id;
  final String status_question;
  final String days_of_waiting;
  final String created;
  final String new_answers_count;
  final String issue_price;

  Posts({this.id, this.header_question, this.city_question, this.text_question, this.mobile_users_id, this.status_question, this.days_of_waiting, this.created, this.new_answers_count, this.issue_price});

  factory Posts.formJson(Map <String, dynamic> json){
  //  print(json.toString());
    return new Posts(
      id: json['id'].toString(),
      header_question: json['header_question'].toString(),
      city_question: json['city_question'].toString(),
      text_question: json['text_question'].toString(),
      mobile_users_id: json['mobile_users_id'].toString(),
      status_question: json['status_question'].toString(),
      days_of_waiting: json['days_of_waiting'].toString(),
      created: json['created'].toString(),
      new_answers_count: json['answersCount'].toString(),
      issue_price: json['issue_price'].toString(),
    );
  }
}
