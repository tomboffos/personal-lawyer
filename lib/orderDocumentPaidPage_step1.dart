import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/AskQuestion_step2.dart';
import 'package:personallawyer/orderDocumentPaidPage_step2.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'lang.dart';

class orderDocumentPaidPage_step1Screen extends StatefulWidget {

  var id;

  orderDocumentPaidPage_step1Screen({Key key, @required this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return orderDocumentPaidPage_step1ScreenState();
  }
}

class orderDocumentPaidPage_step1ScreenState extends State<orderDocumentPaidPage_step1Screen> {

  var data;
  String _lang;

  TextEditingController titleTextDocument = new TextEditingController();
  TextEditingController descTextDocument = new TextEditingController();

  void orderDocumentPaidPage_step1 () async {

    if(titleTextDocument.text.isNotEmpty && descTextDocument.text.isNotEmpty){

      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString('titleTextDocument', titleTextDocument.text);
      sp.setString('descTextDocument', descTextDocument.text);
      sp.setString('categoryDocument', widget.id.toString());

    } else {
      Fluttertoast.showToast(
        msg: trans('please_fill_in_the_fields', _lang),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   _lang = prefs.getString("language");

   setState(() {
     _lang;
   });
  }

  @override
  void initState() {
    super.initState();
    getLang();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('clarification', _lang), style: TextStyle(color: Colors.white)),

        actions: <Widget>[
          FlatButton(
              child: Container(
                child: Text(trans('step1of3', _lang), style: TextStyle(color: Colors.white),),
              )
          )
        ],
      ),

      body:  SingleChildScrollView(
          child: Container (
            padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
            child: Column(
              children: <Widget>[

                Container(
                    padding: EdgeInsets.only(top:5, bottom:10),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: <Widget>[
                        Text(trans('documents_name', _lang),
                          textAlign: TextAlign.start,
                          style: TextStyle(color: Colors.grey,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    )
                ),
                Container(
                  child: TextField(
                    controller: titleTextDocument,
                    textCapitalization: TextCapitalization.sentences,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      hintText: trans('field_to_fill', _lang),
                      hintStyle: TextStyle(color: Colors.grey[200]),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[300]),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
                      ),
                      //labelText: _title,
                      contentPadding: EdgeInsets.all(12.0),
                    ),
                    onChanged: (String text){

                    },
                  ),
                ),

                Container(
                    padding: EdgeInsets.only(top:15, bottom:10),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: <Widget>[
                        Text(trans('detailed_information', _lang),
                          textAlign: TextAlign.start,
                          style: TextStyle(color: Colors.grey,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        Text( '',
                          textAlign: TextAlign.start,
                          style: TextStyle(color: Colors.red,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    )
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 30),
                    // height: 300,
                    child: new ConstrainedBox(constraints: BoxConstraints(
                      // maxHeight: 200.0,
                    ),
                      child: new Scrollbar(
                        child: new SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          //  reverse: true,
                          child: SizedBox(
                            height: 300.0,
                            child: new TextField(
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                hintText: trans('field_to_fill', _lang),
                                hintStyle: TextStyle(color: Colors.grey[200]),
                                filled: true,
                                fillColor: Colors.white,
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[300]),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
                                ),
                                //labelText: _title,
                                contentPadding: EdgeInsets.all(12.0),
                              ),
                              textCapitalization: TextCapitalization.sentences,
                              textInputAction: TextInputAction.done,
                              controller: descTextDocument,
                              maxLines: 80,
                            ),
                          ),
                        ),
                      ),)

                ),


              ],
            ),
          )
      ),

      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: () {
              orderDocumentPaidPage_step1();
              if(titleTextDocument.text.isNotEmpty && descTextDocument.text.isNotEmpty){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => orderDocumentPaidPage_step2Screen()));
              }

            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(right: 15),
                  child: Text(trans('further', _lang),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                ),

                Icon(Icons.keyboard_arrow_right,
                    color: Colors.white
                ),
              ],
            )
        ),
      ),

    );
  }
}