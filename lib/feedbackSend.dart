import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personallawyer/PageView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'lang.dart';
import 'theme_settings.dart';

class FeedbackSend extends StatefulWidget {

  var id;
  var title;

  FeedbackSend({Key key, @required this.id, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FeedbackSendState();
  }
}

class FeedbackSendState extends State<FeedbackSend> {

  var data;

  TextEditingController textDesc = new TextEditingController();

  void Send() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _api = prefs.getString("apikey");

    if(textDesc.text.isNotEmpty){
      var response = await http.post("$Url/api/feedback_massage/massage",
          body:{
            'api_token': _api,
            'header_feedback': widget.title,
            'title_feedback': textDesc.text,
          });

      if(response.statusCode == 200){

        data = jsonDecode(response.body);

        Navigator.push(context, MaterialPageRoute(builder: (context) => PagerScreen(),));

        Fluttertoast.showToast(
          msg: data,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 5,
          backgroundColor: Colors.green,
          textColor: Colors.white,
        );

      }


    } else {
      Fluttertoast.showToast(
        msg: trans('fill_in_all_required_fields', _lang),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }

  }

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    super.initState();
    getLang();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(widget.title, style: TextStyle(color: Colors.white)),
        ),

        body: SingleChildScrollView(
          child: Align(
              alignment: Alignment.center,
              //  padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[

                  Container(
                      margin: EdgeInsets.all(20),
                      height: 200,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(width: 1, color: Color(0xff4267b2),),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.all(10.0),
                      child: new ConstrainedBox(constraints: BoxConstraints(
                        maxHeight: 200.0,
                      ),
                        child: new Scrollbar(
                          child: new SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            //  reverse: true,
                            child: SizedBox(
                              height: 110.0,
                              child: new TextField(
                                textCapitalization: TextCapitalization.sentences,
                                textInputAction: TextInputAction.done,
                                controller: textDesc,
                                maxLines: 80,
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),)

                  ),

                  InkWell(
                    child: Container(
                        width: 200,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(trans('submit', _lang), style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Send();
                    },
                  )

                ],
              )
          ),
        )
    );
  }
}