import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personallawyer/PageView.dart';
import 'package:personallawyer/paybox/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../theme_settings.dart';

class QuestionPay extends StatefulWidget {

  var price;

  QuestionPay({Key key, @required this.price}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return QuestionPayState();
  }
}

class QuestionPayState extends State<QuestionPay> {

  var data;

  String header_question;
  String text_question;
  String user_name;
  String phone_number;
  String user_city;
  String user_email;
  String _api;
  var url;
  String description = "Оплата за вопрос";

  var isLoading = true;

  void getInfo() async {

    SharedPreferences sp = await SharedPreferences.getInstance();

    header_question = sp.getString("titleText");
    text_question = sp.getString('descText');
    user_name = sp.getString('nameText');
    phone_number = sp.getString('phone_number_user');
    user_city = sp.getString('cityText');
    user_email = sp.getString('emailText');
    _api = sp.getString('apikey');

//    print('Title: ' + header_question);
//    print('Description: ' + text_question);
//    print('FIO: ' + user_name);
//    print('Phone: ' + phone_number);
//    print('City: ' + user_city);
//    print('Email: ' + user_email);

  }

  void Pay() async {

    final SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString('apikey');

    url = "$Url/api/$_api/payment/api_paybox_payment/${widget.price}/$description/";

    final response = await http.get(url);

    print(response.body);

    data = jsonDecode(response.body);
    print(data);

    setState(() {
      data;
    });

  }

  void initState() {
    super.initState();
    getInfo();
    Pay();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),

         // leading: Text(''),

          title: Text("Формирование платежа", style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[

                  Container(
                    padding: EdgeInsets.only(top: 50, bottom: 45),
                    child: Column(
                      children: <Widget>[
                        CircularProgressIndicator()
                      ],
                    ),
                  ),

                  Align(
                    child: Text("Счет сформирован",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'MyriadProRegular',
                        color: Color(0xff63ad22),
                      ),) ,
                  ),

                  InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 200,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text('Оплатить', style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
//                      UrlLauncher.launch(data);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => LaunchUrl(url: data,)));
                    },
                  ),



                ],
              ),
            )
        )
    );
  }
}