import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:personallawyer/goodRev.dart';
import 'package:personallawyer/userProfil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'theme_settings.dart';
import 'userProfil.dart';
import 'lang.dart';
import 'feedbackTopic.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';

class ReviewsPageScreen extends StatefulWidget {

  var id;

  ReviewsPageScreen({Key key, @required this.id,}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ReviewsPageScreenState();
  }
}

class ReviewsPageScreenState extends State<ReviewsPageScreen> {

  var data;
  String _lang;

  TextEditingController textDesc = new TextEditingController();

  bool isActiveButton=true;

  void AddReviews() async {
    setState(() {
      isActiveButton=false;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _api = prefs.getString("apikey");

    if(textDesc.text.isNotEmpty){
      var response = await http.post("$Url/api/profile/reviews_add",
          body:{
            'api_token': _api,
            'users_id': widget.id,
            'reviews': textDesc.text,
          });

      if(response.statusCode == 200){

        data = jsonDecode(response.body);
        print('data $data');

//        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
//                builder: (context) => UserProfilScreen()),
//                  (Route<dynamic> route) => false);

      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => goodRevScreen()));

      }


    } else {
      Fluttertoast.showToast(
        msg: trans('fill_in_all_required_fields', _lang),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('feedback3', _lang), style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[

                Container(
                margin: EdgeInsets.all(20),
                height: 200,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: Color(0xff4267b2),),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: EdgeInsets.all(10.0),
                child: new ConstrainedBox(constraints: BoxConstraints(
                  maxHeight: 200.0,
                ),
                  child: new Scrollbar(
                    child: new SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      //  reverse: true,
                      child: SizedBox(
                        height: 200.0,
                        child: new TextField(
                          enableInteractiveSelection: false,
                          textCapitalization: TextCapitalization.sentences,
                          textInputAction: TextInputAction.done,
                          controller: textDesc,
                          maxLines: 100,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                  ),)

                ),

                 // InkWell(
                 //   child: Container(
                 //       width: 200,
                 //       padding: EdgeInsets.all(10),
                 //       decoration: BoxDecoration(
                 //         color: buttom == true ? Colors.green : Colors.grey,
                 //         borderRadius: BorderRadius.circular(10),
                 //       ),
                 //       child: Align(
                 //         alignment: Alignment.center,
                 //         child: Text('Оставить отзыв', style: TextStyle(color: Colors.white),),
                 //       )
                 //   ),
                 //   onTap: () {
                 //     AddReviews();
                 //     buttom = false;
                 //   }
                 // ),

                  Container(
                    width: 200,
                    child: RaisedButton(
                      color: isActiveButton?Colors.green:Colors.grey,
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10),
                      ),
                      child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(trans('leave_feedback', _lang), style: TextStyle(color: Colors.white),),
                          )
                      ),
                      onPressed: isActiveButton?() => AddReviews():()=>{},
                    ),
                  )


           
//                    Column(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: <Widget>[
//
//                        DropdownButtonFormField(
//                          items:  _currencies.map((String dropDownStringItem) {
//                            return DropdownMenuItem <String>(
//                                value: dropDownStringItem,
//                                child: Container(
//                                  child: Text(_currentItemSelected),
//                                //  width: 200,
//                                  alignment: Alignment.center,
//                                ),
//                            );
//                          }).toList(),
//                          onChanged: (String newValueSelected) {
//                            setState(() {
//                              this._currentItemSelected = newValueSelected;
//                            });
//                          },
//                          value: _currentItemSelected,
//                        ),
//
//                      ],
//                    ),




                ],
              ),
            )
        )
    );
  }
}