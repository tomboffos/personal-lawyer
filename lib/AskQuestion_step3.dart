import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:personallawyer/paybox/PayCard.dart';
import 'package:personallawyer/paybox/QuestionPay.dart';
import 'package:personallawyer/paybox/url_launcher.dart';
import 'package:personallawyer/seccessfulPublication.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'lang.dart';
import 'theme_settings.dart';

class AskQuestion_Step3Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AskQuestion_Step3ScreenState();
  }
}

class AskQuestion_Step3ScreenState extends State<AskQuestion_Step3Screen> {
  var data;

  String header_question;
  String text_question;
  String user_name;
  String phone_number;
  String user_city;
  String cityID;
  String user_email;
  String _api;
  var isLoading = true;
  String description = "Оплата за вопрос";
  String _lang;
  String url;
  var services;
  List<Widget> myList = new List<Widget>();
  ListView list;
  var _price = 0;
  bool loadingCardLink = false;
  Map<String,dynamic> card;
  List<dynamic> cards = [];

  bool isChecked = false;
  List<bool> checkboxes = new List<bool>();

//  List<int> prices = new List<int>();
  Map prices = new Map();
  bool loadingPay = false;
  void getInfo() async {
    SharedPreferences sp = await SharedPreferences.getInstance();

    header_question = sp.getString("titleText");
    text_question = sp.getString('descText');
    user_name = sp.getString('nameText');
    phone_number = sp.getString('phone_number_user');
    user_city = sp.getString('cityText');
    user_email = sp.getString('emailText');
    cityID = sp.getString('cityID');

    print('cityID: ' + cityID);
    print('user_city: ' + user_city);
  }

  void sendRequest() async {
    print(_api);
    print(header_question);
    print(text_question);
    print(user_name);
    print(phone_number);
    print(cityID);
    print(user_email);
    print(_price);

    print(prices);

    Map<String, dynamic> params = {
      'api_token': _api,
      'header_question': header_question,
      'text_question': text_question,
      'user_name': user_name,
      'phone_number': phone_number,
      'user_city': cityID,
      'user_email': user_email,
      'issue_price': jsonEncode(prices),
    };
    final response = await http.post(
        '$Url/api/question/add_question',
        body: params);

    print(params);
    print('response: ${jsonDecode(response.body)}');

    print(jsonEncode(_price));
    print(response.statusCode);
    if (response.statusCode == 200) {
//      Navigator.push(
//          context,
//          MaterialPageRoute(
//              builder: (context) => seccessfulPublicationScreen()));

      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString('id', response.body);

      Pay();

//      Fluttertoast.showToast(
//        msg: jsonDecode(response.body),
//        timeInSecForIos: 3,
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        textColor: Colors.white,
//        backgroundColor: Colors.green,
//      );
    } else {
      print(jsonEncode(prices));
      print(_api);
      print(response.body);
    }
  }

  void getCards()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/payment/cards";
    final response = await http.get(url);
    final body =  jsonDecode(response.body);

    print(response.body);

    setState(() {
      cards = body['cards'];
      print(cards);
    });


  }
  bool chooseCard = false;

  void getServices() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url =
        "$Url/api/$_api/question/valuable/questions/$_lang";

    print(url);

    final response = await http.get(url);
    print(response.body);
    services = jsonDecode(response.body);

    setState(() {
      services;
      isLoading = false;
    });
    print('services ${services.length}');

//    for (int i = 0; i < services.length; i++) {
//
//    checkboxes.add(false);
//
//      myList.add(
//          new Column(
//            children: <Widget>[
//              Card(
//                child: new CheckboxListTile(
//                  title: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
//                      Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
//                    ],
//                  ),
//                 // value: checkboxes[i],
//                  value: checkboxes[i],
//                  onChanged: (value) {
//                    if(checkboxes[i] == false){
//                      prices["checkbox_"+services[i]['id'].toString()] = services[i]['id'];
//                      setState(() {
//                        _price += services[i]['are_valuable'];
//                        checkboxes[i] = true;
//                        print(prices.toString());
//                      });
//                    } else{
//                      prices.remove('checkbox_'+services[i]['id'].toString());
//                      setState(() {
//                        _price -= services[i]['are_valuable'];
//                        checkboxes[i] = false;
//                        print(prices.toString());
//                      });
//                    }
//                  },
//                ),
//              ),
//
// //
//
//            ],
//          )
//      );
//    }
  }

  void Pay() async {
    final SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString('apikey');
    var id = sp.getString('id');

    print(id);

    url =
        "$Url/api/$_api/payment/api_paybox_payment/$id/questions/";

    print(url);

    final response = await http.get(url);

    data = jsonDecode(response.body);
    print(data);

    setState(() {
      data;

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LaunchUrl(
                    url: data,
                  )));
    });
  }
  void linkCard()async{
    setState(() {
      loadingCardLink = true;
    });
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    url = "https://advokat.vipsite.kz/api/$_api/payment/link-card";
    print(url);
    final response =await http.post(url);
    final body  = jsonDecode(response.body);
    setState(() {
      loadingCardLink = false;
    });
    print(body);
    Navigator.push(context,MaterialPageRoute(builder: (context)=>LaunchUrl(url:body['response']['pg_redirect_url'])));


  }
  todayDate() {
    DateTime now = new DateTime.now();
    DateTime min = DateTime.parse('1900-01-01 20:00');
    DateTime max = DateTime.parse('1900-01-01 09:00');
    print(now);
    print(min);
    print(max);
    print(now.hour > max.hour && now.hour < min.hour);

    if((now.hour > max.hour && now.hour < min.hour) == false) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[

                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                      child: Text('Ваша заявка будет обработана в рабочее время. Вы готовы ждать?', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
                    ),

                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(right: 5),
                              padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                              decoration: BoxDecoration(
                                  color: Color(0xff4267b2),
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Text('Да', style: TextStyle(color: Colors.white),),
                            ),
                            onTap: (){
                              if (_price > 0) {
                                sendRequest();
                                print(_price);
                              }
                            },
                          ),

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(left: 5),
                              padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                              decoration: BoxDecoration(
                                  color: Color(0xff4267b2),
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Text('Нет', style: TextStyle(color: Colors.white),),
                            ),
                            onTap: (){
                              Navigator.pop(context);
                            },
                          )

                        ],
                      ),
                    )

                  ],
                ),
              )
            ]);
          });
    } else {
      if (_price > 0) {
        sendRequest();
        print(_price);
      }
    }



    //   getLinkPay();


  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    super.initState();
    getLang();
    getInfo();
    getCards();
    getServices();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(trans('question_type', _lang),
            style: TextStyle(color: Colors.white)),
        actions: <Widget>[
          FlatButton(
              child: Container(
            child: Text(
              trans('step3of3', _lang),
              style: TextStyle(color: Colors.white),
            ),
          ))
        ],
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: 15.0, right: 15.0, top: 15, bottom: 10),
                  child: Text(
                    trans('what_do_you_want_to_order', _lang),
                    style: TextStyle(fontSize: 17),
                    textAlign: TextAlign.left,
                  ),
                ),

                // Column(
                //   children: myList,
                // ),

                Expanded(
                  child: Column(
                    children: [
                      Container(
                        height:100,
                        padding: EdgeInsets.only(right: 15, left: 15),
                        child: ListView.builder(
                            itemCount: services.length,
                            itemBuilder: (context, i) {
//                      prices.add(0);
//                      checkboxes.add(false);
                              return new Card(
                                child: CheckboxListTile(
                                    title: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(services[i]["service"], style: TextStyle(color: Colors.black54),),
                                        Text(services[i]["are_valuable"].toString() + ' тенге', style: TextStyle(color: Colors.green),),
                                      ],
                                    ),
                                    // subtitle: Text(
                                    //   services[i]["are_valuable"].toString() +
                                    //       trans('tenge', _lang),
                                    //   style: TextStyle(color: Colors.black),
                                    // ),
                                    value: services[i]['isCheck'],
                                    onChanged: (value) {
                                      setState(() {
                                        if (services[i]['isCheck'] == false) {
                                          prices["checkbox_" +
                                                  services[i]['id'].toString()] =
                                              services[i]['id'];
                                          _price += services[i]['are_valuable'];
                                          services[i]['isCheck'] = true;
                                          //                        checkboxes[i] = true;
                                        } else {
                                          prices.remove('checkbox_' +
                                              services[i]['id'].toString());
                                          _price -= services[i]['are_valuable'];
                                          services[i]['isCheck'] = false;
                                          //                        checkboxes[i] = false;
                                        }
                                      });
                                    }),
                              );
                            }),
                      ),
                      GestureDetector(

                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: ()async{
                                if(loadingCardLink == false){
                                  await getCards();
                                  if(cards.length == 0){
                                    linkCard();

                                  }else{
                                    chooseCard = !chooseCard;

                                  }
                                }

                              },

                              child: Container(
                                padding: const EdgeInsets.only(top:20,left:20,right:20),
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black,),
                                      borderRadius: BorderRadius.circular(10)


                                  ),
                                  height: 50,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Center(
                                        child: Text(card != null ? card['card_hash'] :'Выбрать карту'),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            chooseCard == true ? Container(
                              padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10),

                              child: Container(
                                height: 150,
                                child: ListView.builder(
                                    itemCount: cards.length,
                                    itemBuilder: (context,index){
                                      return Column(
                                        children: [
                                          GestureDetector(
                                            onTap:(){
                                              setState(() {
                                                chooseCard = false;
                                                card = cards[index];
                                                print(card);
                                              });

                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: index != 0 ? Border(top: BorderSide(color: Colors.black )) : Border(top:BorderSide(color:Colors.white)),
                                              ) ,

                                              padding: const EdgeInsets.symmetric(vertical: 15,),
                                              child: Center(
                                                child: Text('${cards[index]['card_hash']}',style: TextStyle(fontSize: 15),),

                                              ),
                                            ),
                                          ),
                                          index == cards.length-1 ?
                                          GestureDetector(
                                            onTap:()async{
                                              linkCard();
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: cards.length != 0 ? Border(top: BorderSide(color: Colors.black )) : Border.all(color:Colors.white),
                                              ) ,

                                              padding: const EdgeInsets.symmetric(vertical: 15,),
                                              child: Center(
                                                child: Text('Добавить карту',style: TextStyle(fontSize: 15),),

                                              ),
                                            ),
                                          ): SizedBox.shrink()

                                        ],
                                      );
                                    }
                                ),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                    borderRadius: BorderRadius.circular(10)
                                ),
                              ),
                            ) : SizedBox.shrink()
                          ],
                        ),
                      ),

                    ],
                  ),
                ),


                _price!=0? Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                      padding: EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 15, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Text(
                            trans('total', _lang),
                            style: TextStyle(fontSize: 17),
                            textAlign: TextAlign.left,
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              '${_price}' + trans('tenge', _lang),
                              style: TextStyle(fontSize: 17),
                              textAlign: TextAlign.left,
                            ),
                          )
                        ],
                      )),
                ):Container(),

//                  Container(
//                    padding: EdgeInsets.only(top: 20, left: 15, right: 15),
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        InkWell(
//                          child: Container(
//                              width: 200,
//                              padding: EdgeInsets.all(10),
//                              decoration: BoxDecoration(
//                                color: Colors.green,
//                                borderRadius: BorderRadius.circular(10),
//                              ),
//                              child: Align(
//                                alignment: Alignment.center,
//                                child: Text('Оплатить', style: TextStyle(color: Colors.white),),
//                              )
//                          ),
//                          onTap: (){
//
//                          },
//                        ),
//                      ],
//                    ),
//                  )
              ],
            ),
          ),
      //   floatingActionButton: FloatingActionButton(
      //     child: Icon(Icons.add),
      //     onPressed: () { print('Clicked'); },
      //   ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(

            onPressed: () {
              print(loadingPay);

              if(loadingPay == false){
                setState(() {
                  loadingPay = true;
                });
                if(_price > 0){
                  if(card != null){

                    sendCardRequest();
                    print('lol');
                  }
                }

              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_balance_wallet, color: Colors.white),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(trans('to_pay', _lang),
                      style: TextStyle(color: Colors.white, fontSize: 15)),
                )
              ],
            )),
      ),
    );
  }
  void sendCardRequest() async{
    setState(() {
      loadingPay = true;
    });
    final response = await http.post('https://advokat.vipsite.kz/api/question/add_question',
        body: {
          'api_token': _api,
          'header_question': header_question,
          'text_question': text_question,
          'user_name': user_name,
          'phone_number': phone_number,
          'user_city': cityID,
          'user_email': user_email,
          'issue_price': jsonEncode(prices),
        });



    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('id', response.body);

    if(response.statusCode == 200) {
      final url = 'https://advokat.vipsite.kz/api/$_api/payment/pay-by-card';
      var id = sp.getString('id');
      print(id);

      final payResponse = await http.post(url,body: {
        'user_card_id' : card['id'].toString(),
        'pay_id' : id,
        'description' : 'questions'
      });
      final mainPayment = jsonDecode(payResponse.body)['payment'];
      Navigator.push(context,MaterialPageRoute(builder: (context)=> PayCard(htmlData:mainPayment,isDocumentSale: false,)));

    }


  }

}
