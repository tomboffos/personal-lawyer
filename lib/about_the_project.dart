import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:share/share.dart';
import 'lang.dart';
import 'package:map_launcher/map_launcher.dart';

class AboutTheProjectScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AboutTheProjectScreenState();
  }
}

class AboutTheProjectScreenState extends State<AboutTheProjectScreen> {

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  openMapsSheet(context) async {
    try {
      final coords = Coords(43.230141,76.8851051);
      final title = "Personal Lawyer.";
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                        onTap: () => map.showMarker(
                          coords: coords,
                          title: title,
                        ),
                        title: Text(map.mapName),
                        leading: Image(
                          image: map.icon,
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    } catch (e) {
      print(e);
    }
  }

  void initState() {
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('about_the_project', _lang), style: TextStyle(color: Colors.white)),
      ),

      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Container(
                padding: EdgeInsets.only(top: 10, right: 15, left: 15),
                child: Text(trans('about_text1', _lang), style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17),),
              ),

              Container(
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(width: 1.0, color: Colors.grey[300]),
                      bottom: BorderSide(width: 1.0, color: Colors.grey[300]),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(trans('corporate_disputes', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('tax_disputes', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('tax_disputes', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('debt_collection', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('family_disputes', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('land_disputes', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('registration_re-registration_of_firms', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('intellectual_property_and_copyright', _lang)),
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2,),
                      ),
                      Text(trans('apostille', _lang)),
                    ],
                  )
              ),

              Container(
                padding: EdgeInsets.only( right: 15, left: 15),
                child: Text(trans('to_contact_us', _lang), style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17),),
              ),

              Container(
                  margin: EdgeInsets.only(top: 10),
                  padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(width: 1.0, color: Colors.grey[300]),
                      bottom: BorderSide(width: 1.0, color: Colors.grey[300]),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[



                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.phone_android),
                            Container(
                              padding: EdgeInsets.only(left: 10, right: 5),
                              child: Text(trans('telephone', _lang), style: TextStyle(fontWeight: FontWeight.w600),),
                            ),
                            Text('+7 775 901 95 55')
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2, color: Colors.grey[300],),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.mail_outline),
                            Container(
                              padding: EdgeInsets.only(left: 10, right: 5),
                              child: Text('E-mail:', style: TextStyle(fontWeight: FontWeight.w600),),
                            ),
                            Text('info@personal-lawyer.kz')
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2, color: Colors.grey[300],),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.check_circle_outline),
                            Container(
                              padding: EdgeInsets.only(left: 10, right: 5),
                              child: Text('Web-site:', style: TextStyle(fontWeight: FontWeight.w600),),
                            ),
                            Text('www.personal-lawyer.kz')
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Divider(height: 2, color: Colors.grey[300],),
                      ),

                       Container(
                         padding: EdgeInsets.only(top: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.location_on),
                            Container(
                              padding: EdgeInsets.only(left: 10, right: 5),
                              child: Text(trans('address', _lang), style: TextStyle(fontWeight: FontWeight.w600),),
                            ),
                            Expanded(
                              child: Text(trans('address2', _lang)),
                            )
                          ],
                        ),
                      ),


                    ],
                  )
              ),

              Container(
                margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  color: Color(0xff4267b2),
                  child: Text(trans('view_on_the_map', _lang), style: TextStyle(color: Colors.white),),
                  onPressed: () => openMapsSheet(context),
                ),
              )

            ],
          ),
        )
      ),

    );
  }
}