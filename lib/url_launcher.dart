import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:personallawyer/lang.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class LaunchUrl extends StatefulWidget {
  final String url;

  LaunchUrl({Key key, @required this.url}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LaunchUrlState();
  }
}

class LaunchUrlState extends State<LaunchUrl>{
  final fwvp = new FlutterWebviewPlugin();
  bool done = false;
  bool call = false;
  String _lang;
  String _num;
  String _call = "";

  @override
  void initState() {
    startCounter();
    super.initState();
  }

  @override
  void dispose() {
    fwvp.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text("ТОО «IT Group Kazakhstan»", style: TextStyle(color: Colors.white)),
      ),
      body: Column(children: <Widget>[
        Container(
          width: double.infinity,
          // height:15,
          color: Colors.white,
        ),
        Container(
          child: done
              ? Container()
              : LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            valueColor:
            AlwaysStoppedAnimation<Color>(Colors.blue),
          ),
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: WebviewScaffold(
              url: widget.url,
              withZoom: true,
            )),
      ]),
    );
  }

  void startCounter() {
    Future.delayed(const Duration(seconds: 4), () {
      // deleayed code here
      setState(() {
        done = true;
      });
    });
  }
}
