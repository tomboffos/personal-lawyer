import 'package:flutter/material.dart';
import 'package:personallawyer/orderDocumentFreeList.dart';
import 'orderDocumentPaidCategory.dart';


class orderDocumentScreen extends StatefulWidget{

  var title;

  orderDocumentScreen({Key key, @required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentScreenState();
  }
}


class orderDocumentScreenState extends State<orderDocumentScreen> with SingleTickerProviderStateMixin{

  TabController tabC;

  @override
  void initState(){
    super.initState();
    tabC = TabController(length: 2, vsync: this, initialIndex: 0);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Text(widget.title,
          style: TextStyle(
              color: Colors.white
          ),
        ),
        //iconTheme: new IconThemeData(color: Colors.black),
        actions: <Widget>[
        ],

      ),

  body: orderDocumentPaidCategoryScreen(),

  /*
    body: new DefaultTabController(
        length: 2,
        child: new Column(
          children: <Widget>[
            new Container(
              constraints: BoxConstraints(maxHeight: 150.0),
              child: new Material(
                color: Colors.white,
                child: new TabBar(
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(width: 2, color: Colors.green,),
                    ),
                    //indicatorColor: Colors.green,
                    labelColor: Colors.green,
                    unselectedLabelColor: Colors.grey,
                    labelStyle: TextStyle(fontSize: 17),
                    tabs: [
                      Tab(text: 'Платный'),
                      Tab(text: 'Бесплатный'),
                ]),
              ),
            ),


            new Expanded(
                child: new TabBarView(
                    children: [
                      orderDocumentPaidCategoryScreen(),
                      orderDocumentFreeListScreen(),
                    ]))
          ],
        )),

      */

    );
  }

}