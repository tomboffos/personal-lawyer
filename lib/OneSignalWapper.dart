import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'myQuestionPage.dart';

class OneSignalWapper {
  static void handleClickNotification(BuildContext context) {
    OneSignal.shared.setNotificationOpenedHandler(
        (OSNotificationOpenedResult result) async {
      try {
        String result_page = result.notification.payload
            .additionalData['page'];
        int question_id;
        if (result_page == 'my_questions') {

          question_id =
          result.notification.payload.additionalData['question_id'];
          Navigator.of(context).push(

            MaterialPageRoute(
              builder: (context) =>
                  myQuestionPageScreen(id_question: '${question_id}'),
            ),
          );
        }else if (result_page == 'main') {

        }

      } catch (e, stacktrace) {

      }
    });
  }
}
