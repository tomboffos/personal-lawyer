
import 'dart:convert';

Cities citiesFromJson(String str) => Cities.fromJson(json.decode(str));


class Cities {
  Cities({
    this.cities,
  });

  List<City> cities;

  factory Cities.fromJson(Map<String, dynamic> json) => Cities(
    cities: List<City>.from(json["cities"].map((x) => City.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
  };
}

class City {
  City({
    this.id,
    this.cities,
    this.nameRu,
    this.nameKz,
  });

  int id;
  String cities;
  String nameRu;
  String nameKz;

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"],
    cities: json["cities"],
    nameRu: json["nameRu"] == null ? null : json["nameRu"],
    nameKz: json["nameKz"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "cities": cities,
    "nameRu": nameRu == null ? null : nameRu,
    "nameKz": nameKz,
  };
}
