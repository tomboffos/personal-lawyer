import 'package:flutter/material.dart';
import 'package:personallawyer/orderDocumentPaidPage_step1.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import 'theme_settings.dart';


class orderDocumentPaidCategoryScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentPaidCategoryScreenState();
  }
}


class orderDocumentPaidCategoryScreenState extends State<orderDocumentPaidCategoryScreen>{

  String _api;
  var url;
  var category;
  String _lang;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void getListCategory() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "$Url/api/$_api/document/categories_paid_document/$_lang/";

    final response = await http.get(url);

    print(_api);


    category = jsonDecode(response.body);

    print(category);

    setState(() {
      category;
      isLoading = false;
    });

    for (int i = 0; i < category.length; i++) {
      myList.add(
          new Column(
            children: <Widget>[
              ListTile(
                title: Text(category[i]['categories']),
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey,),
                onTap: (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => orderDocumentPaidPage_step1Screen(id: category[i]["id"])));
                },
              ),
              Divider(height: 2,),
            ],
          )

      );
    }

  }


  void initState() {
    getListCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  isLoading
          ? Center(
          child: CircularProgressIndicator())
          : SingleChildScrollView(
        child: Container(
            child: Column(
              children: myList,
            ),
        ),
      )

    );
  }

}