import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:personallawyer/AskQuestion_step1.dart';
import 'package:personallawyer/PageView.dart';
import 'package:personallawyer/RateTheApplication.dart';
import 'package:personallawyer/about_the_project.dart';
import 'package:personallawyer/allQuestions.dart';
import 'package:personallawyer/feedback.dart';
import 'package:personallawyer/finishedDocuments.dart';
import 'package:personallawyer/main.dart';
import 'package:personallawyer/myquestion.dart';
import 'package:personallawyer/orderDocument.dart';
import 'package:personallawyer/orderDocumentFreeList.dart';
import 'package:personallawyer/orderDocumentPaidCategory.dart';
import 'package:personallawyer/profile.dart';
import 'package:personallawyer/reviewsList.dart';
import 'package:personallawyer/settings.dart';
import 'package:personallawyer/tellaboutus.dart';
import 'feedbackTopic.dart';
import 'pravoved_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'lang.dart';
import 'theme_settings.dart';
import 'url_launcher.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';
import 'package:path/path.dart' as PATH;
import 'package:async/async.dart';
import 'package:http/http.dart';
import 'dart:convert';

class NavDrawer extends StatefulWidget{

  String title;
  final count;
  NavDrawer({Key key, @required this.title, this.count}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NavDrawerState();
  }
}

class NavDrawerState extends State<NavDrawer> {

  TextEditingController passC = new TextEditingController();


  String _lang;

  bool kaz = false;
  bool rus = false;

  String _api;
  var url = '';
  var data = '';
  var isLoading = true;
  var user = '';
  String _number;
  String _user_number;
  String _user_image = '';
  String _user_link = '';
  var _user_id;
  var AvatarUrl;

  void getInfo() async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _number = sp.getString("phone_number");
    _user_link = sp.getString("user_image");
    _user_number = sp.getString('phone_number_user');
    _api = sp.getString("apikey");

    _user_image = '$Url/' + _user_link;
  }

  void setLang(String s) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("language", s);
    setButton(s);
  }

  setButton(String i) async{
    setState(() {
      if(i == "kaz"){
        kaz = true;
        rus = false;
      } else if (i == "rus") {
        kaz = false;
        rus = true;
      }
    });
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    if (_lang == null || _lang == "") {
      _lang = "rus";
    }

    await prefs.setString("language", _lang);

    setState(() {
      _lang;
    });
  }

  @override
  void initState(){
    super.initState();

    getLang();
    getInfo();
  }

  final picker = ImagePicker();
  File avatar;
  String _opGenderValue;

  Future getImage(bool fromCamera) async {
    var pickedFile;
    if(fromCamera) {
      pickedFile = await picker.getImage(source: ImageSource.camera);
    } else {
      pickedFile = await picker.getImage(source: ImageSource.gallery);
    }

    setState(() {
      avatar = File(pickedFile.path);
      Toast.show(
          'Фотография загружена',
          context,
          duration: 2,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          gravity: Toast.BOTTOM
      );
    });
    uploadAvatar(avatar: avatar);
    Navigator.of(context).pop();
  }

  void getFileDialog() {
    showDialog(
      context: context,
      barrierDismissible: true,
      child:Dialog(
        child: Container(
          height: 130,
          padding: EdgeInsets.all(5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  child: ListTile(
                    onTap: () => getImage(true),
                    leading: Icon(Icons.camera_alt, color: Color(0xff4267b2),),
                    title: Text('Сделать фотографию', style: TextStyle(fontSize: 15)),
                  )
              ),
              Divider(height: 2,),
              Container(
                  child: ListTile(
                    onTap: () => getImage(false),
                    leading: Icon(Icons.image, color: Color(0xff4267b2),),
                    title: Text('Выбрать из галереи', style: TextStyle(fontSize: 15)),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }


//  void getLang() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    _lang = prefs.getString("language");
//    print(_lang + ' Igor\'s null');
//  }

  @override
  Widget build(BuildContext context) {
   getLang();
    return Drawer(
      child: Container(
        color: Color(0xff4b4b53),
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[

            Container(
              height: 160,
              child: DrawerHeader(
                // padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[

                          GestureDetector(
                            onTap: () => getFileDialog(),
                            child:
                            Container(
//                              height: 150,
//                              width: 150,
                              child: CircularProfileAvatar(
                                "$_user_image",
                                radius: 40,
                                backgroundColor: Colors.transparent,
                                borderWidth: 3,
                                //  initialsText: Text("SC", style: TextStyle(fontSize: 40, color: Color(0xff4b4b53),),),
                                borderColor: Colors.white,
                                elevation: 5.0,
                                //foregroundColor: Colors.white10.withOpacity(0.5),
                                cacheImage: true,
                                showInitialTextAbovePicture: true,
                              )
                            ),
                          ),
//                          Container(
//                              padding: EdgeInsets.only(top:20.0, bottom: 15.0, left: 0),
//                              child:  CircularProfileAvatar(
//                                _user_image == null || _user_image == "" ?
//                                'https://kolesogizni.com/images/author_img/author-without-photo.jpg'
//                              : 'https://advokat.vipsite.kz/$_user_image',
//                                radius: 40,
//                                backgroundColor: Colors.transparent,
//                                borderWidth: 3,
//                              //  initialsText: Text("SC", style: TextStyle(fontSize: 40, color: Color(0xff4b4b53),),),
//                                borderColor: Colors.white,
//                                elevation: 5.0,
//                                //foregroundColor: Colors.white10.withOpacity(0.5),
//                                cacheImage: true,
//                                onTap: () {
//                                  print('adil');
//                                },
//                                showInitialTextAbovePicture: true,
//                              )
//                          ),
                          Container(
                            padding: EdgeInsets.only(left: 15, right: 25),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[

                              /*  Container(
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  child: Text("Личный" + ' ' + "Адвокат", style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),),
                                ),*/

//                                GestureDetector(
//                                  child: Text(trans('settings', _lang), style:TextStyle(
//                                    fontSize: 14,
//                                    color: Colors.white,
//                                  ),),
//                                  onTap: () {
//                                    Navigator.push(
//                                        context,
//                                        MaterialPageRoute(
//                                            builder: (context) => SettingsScreen()));
//                                  },
//                                ),

                                GestureDetector(
                                  child: _user_number == null ? Container() : Text(_user_number, style: TextStyle(color: Colors.white),),
                                ),

                                Container(
                                  padding: EdgeInsets.only(top: 10.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[

                                      InkWell(
                                        child: Container(
                                          padding: EdgeInsets.only(right: 10),
                                          child: Text(
                                            "ҚАЗ",
                                          style: TextStyle(
                                          color: _lang == "kaz" ? Colors.white : Color(0xff4267b2),
                                          fontWeight: _lang == "kaz" ? FontWeight.bold : FontWeight.normal,
                                          fontSize: 13,
                                            ),
                                          ),
                                        ),
                                       onTap: () {
                                            setLang("kaz");
                                       },
                                      ),

                                      InkWell(
                                        child: Container(
                                          padding: EdgeInsets.only(left: 10),
                                          child: Text(
                                            "РУС",
                                            style: TextStyle(
                                              color: _lang == "rus" ? Colors.white : Color(0xff4267b2),
                                              fontWeight: _lang == "rus" ? FontWeight.bold : FontWeight.normal,
                                              fontSize: 13,
                                            ),
                                          ),
                                        ),
                                        onTap: () {
                                          setLang("rus");
                                        },
                                      ),


                                    ],
                                  ),
                                )

                              ],
                            ),
                          ),
                          Expanded(
                            //  flex: 1,
                              child: IconButton(
                                icon: Icon(Icons.exit_to_app,
                                  color: Colors.white,
                                ),
                                onPressed: (){
                                  logOut();
                                },
                              )
                          ),
                        ],
                      ),
                    ),

                  ],
                ),

                decoration: new BoxDecoration(
                  gradient: LinearGradient(
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter,
                      colors: [
                        Color(0xff36363c),
                        Color(0xff36363c),
                      ],
                      tileMode: TileMode.repeated),
                ),
              ),
            ),

            Container(
              color: Color(0xff4b4b53),
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[

                  ListTile(
                    leading: Icon(Icons.question_answer,
                      color: Colors.white,),
                    title: Text(trans('my_questions', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyQuestionScreen(title: trans('my_questions', _lang),)));
                    },
                    trailing: Text('${this.widget.count}',style:TextStyle(color: Colors.white)),

                  ),
                  Divider(height: 2.0, color: Colors.white,),

                  ListTile(
                    leading: Icon(Icons.archive,
                      color: Colors.white,),
                    title: Text(trans('ready_documents', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FinishedDocumentsScreen(title: trans('ready_documents', _lang),)));
                    },
                  ),
                  Divider(height: 2.0, color: Colors.white),

                  ListTile(
                    leading: Icon(Icons.info,
                      color: Colors.white,),
                    title: Text(trans('about_the_project', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AboutTheProjectScreen()));
                    },
                  ),
                  Divider(height: 2.0, color: Colors.white,),

                  ListTile(
                    leading: Icon(Icons.chat,
                      color: Colors.white,),
                    title: Text(trans('reviews4', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => reviewsList()));
                    },
                  ),
                  Divider(height: 2.0, color: Colors.white,),

//                  ListTile(
//                    leading: Icon(Icons.receipt,
//                      color: Colors.white,),
//                    title: Text(trans('order_documents', _lang), style: TextStyle(color: Colors.white),),
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => orderDocumentScreen(title: trans('order_documents', _lang),)));
//                    },
//                  ),
//                  Divider(height: 2.0, color: Colors.white),

//                  ListTile(
//                    leading: Icon(Pravoved.vopros,
//                      color: Colors.white,),
//                    title: Text(trans('request_a_consultation', _lang), style: TextStyle(color: Colors.white),),
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => AskQuestion_Step1Screen()));
//                    },
//                  ),
//                  Divider(height: 2.0, color: Colors.white),



//                  ListTile(
//                    leading: Icon(Pravoved.folderber,
//                      color: Colors.white,),
//                    title: Text(trans('questions_on_the_project', _lang), style: TextStyle(color: Colors.white),),
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => AllQuestionsScreen(title: trans('questions_on_the_project', _lang))));
//                    },
//                  ),
//                  Divider(height: 2.0, color: Colors.white,),

                  ListTile(
                    leading: Icon(Icons.settings,
                      color: Colors.white,),
                    title: Text(trans('settings', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                              Navigator.push(
                              context,
                                  MaterialPageRoute(
                          builder: (context) => SettingsScreen()));
                    },
                  ),
                  Divider(height: 2.0, color: Colors.white,),

                  ListTile(
                    leading: Icon(Icons.email,
                      color: Colors.white,),
                    title: Text(trans('feedback', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FeedbackTopicScreen()));
                    },
                  ),
                  Divider(height: 2.0, color: Colors.white,),

                  ListTile(
                    leading: Icon(Icons.redo,
                      color: Colors.white,),
                    title: Text(trans('tell_us_about_us', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TellAboutUsScreen()));
                    },
                  ),
                  Divider(height: 2.0, color: Colors.white,),

                  ListTile(
                    leading: Icon(Icons.grade,
                      color: Colors.white,),
                    title: Text(trans('rate_the application', _lang), style: TextStyle(color: Colors.white),),
                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => RateTheApplicationScreen()));
                      showReviewDialog();
                    },
                  ),

//                  ListTile(
//                    leading: Icon(Pravoved.star,
//                      color: Colors.white,),
//                    title: Text('fdfdfdfdfd', style: TextStyle(color: Colors.white),),
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => orderDocumentFreeListScreen()));
//                    },
//                  ),

               /*  Container(
                   color: Color(0xff4267b2),
                   child:  ListTile(
                     leading: Icon(Pravoved.call,
                       color: Colors.white,),
                     title: Text("$_number\n" + _free_consultation, style: TextStyle(color: Colors.white),),
                     onTap: () {
                       UrlLauncher.launch("tel:$_number");
                     },
                   ),
                 ), */

                 InkWell(
                   child: Container(
                     color: Color(0xff36363c),
                     child:  ListTile(
                       title: Text(trans('developed_by', _lang) + "itgroup.kz", style: TextStyle(color: Color(0xff858593), fontSize: 16, fontFamily: 'MyriadProRegular'), textAlign: TextAlign.center,),

                     ),
                   ),
                   onTap: (){
                     Navigator.push(context, MaterialPageRoute(builder: (context) => LaunchUrl(url: "https://itgroup.kz")));
                    // Navigator.push(context, PageRouteBuilder(pageBuilder: (context, _, __) => LaunchUrl(url: "https://itgroup.kz",)));
                   },
                 )


                ],
              ),
            )


          ],
        ),
      )
    );
  }

  void showReviewDialog() {
    final InAppReview inAppReview = InAppReview.instance;
    inAppReview.openStoreListing(appStoreId: '1499337941');
  }

  void logOut() async{

    SharedPreferences sp = await SharedPreferences.getInstance();
    String cityCity = sp.getString("cityText");
    String cityID = sp.getString('cityID');
    sp.clear();


    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("logged", false);
    await prefs.setString('cityText', cityCity);
    await prefs.setString('cityID', cityID);

    Route route = MaterialPageRoute(builder: (context) => MyHomePage());
    Navigator.pushReplacement(context, route);
  }



  Future uploadAvatar({String id, File avatar}) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _user_id = sp.get('user_id');
    _api = sp.getString("apikey");

    Map<String, dynamic> _data = new Map<String, String>();

    var url = Uri.parse('$Url/api/user-auth/avatar');

    var request = new MultipartRequest('POST', url);

    _data['id'] = _user_id.toString();
    _data['api_token'] = _api;

    if(avatar != null && avatar.path != null) {
      var stream = new ByteStream(DelegatingStream.typed(avatar.openRead()));
      var length = await avatar.length();
      var multipartFile = new MultipartFile('avatar', stream, length,
          filename: PATH.basename(avatar.path));
      request.files.add(multipartFile);
    }

    request.fields.addAll(_data);

    var response = await request.send();

    response.stream.transform(utf8.decoder).listen((response) async {
      var serverResponse = jsonDecode(response);

      AvatarUrl = serverResponse['avatar'];

      setState(() {
        _user_image = AvatarUrl;
      });
    });

  }


}
