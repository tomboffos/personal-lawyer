import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:personallawyer/main-star.dart';
import 'package:personallawyer/main-start-smscode.dart';
import 'NavigationService.dart';
import 'OneSignalWapper.dart';
import 'PageView.dart';
import 'contractDoc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'editProfile.dart';
import 'myQuestionPage.dart';
import 'myquestion.dart';
import 'notifications.dart';
import 'profileEdit.dart';
import 'theme_settings.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:no_context_navigation/no_context_navigation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Widget startScreen;
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.getBool("logged") != null && prefs.getBool("logged")) {
    startScreen = PagerScreen();
  } else {
    startScreen = MyHomePage(title: 'Personal Lawyer');
  }

  runApp(MyApp(startScreen: startScreen));
}

class MyApp extends StatelessWidget {
  Widget startScreen;

  MyApp({
    this.startScreen
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      navigatorKey: NavigationService.navigationKey,
      debugShowCheckedModeBanner: false,
      title: 'Personal Lawyer',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color(0xff4267b2),
      ),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {

          case '/myQuestion':
            return MaterialPageRoute(builder: (_) => myQuestionPageScreen(id_question: settings.arguments));
          default:
            return null;
        }
      },

      home: startScreen,
    );

  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController _phoneC =
  new MaskedTextController(mask: '+7(000)000-00-00');

  var _isLoading = false;
  final NavigationService navService = NavigationService();

  static final FacebookLogin facebookSignIn = new FacebookLogin();

  double setTop() {
    if (MediaQuery
        .of(context)
        .size
        .width *
        MediaQuery
            .of(context)
            .devicePixelRatio >
        700) {
      return 50;
    } else {
      return 65;
    }
  }

  @override
  void didChangeDependencies() {

  }

  String _lang;

  void initOneSignal() {
    OneSignal.shared.init("70631e06-e94f-4e8b-a74f-7588690a6546");
    OneSignal.shared.setInFocusDisplayType(
        OSNotificationDisplayType.notification);
    OneSignal.shared.setNotificationOpenedHandler((
        OSNotificationOpenedResult result) {
      // will be called whenever a notification is opened/button pressed.
      // page: "my_questions", question_id: "<id>"
      // когда приходят уведомления об ответе на обращение
      // page: "main" когда приходят массовое уведомление из админки


      // {Time: 11:49:54, page: my_questions, question_id: 319, Date: 21-10-2020, Key: true}

      String result_page = result.notification.payload
          .additionalData['page'];
      int question_id;
      if (result_page == 'my_questions') {
        question_id =
        result.notification.payload.additionalData['question_id'];
        navService.pushNamed('/myQuestion', args: '${question_id}');
      } else if (result_page == 'main') {

      }
    });

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    if (_lang == null || _lang == "") {
      _lang = "rus";
    }

    await prefs.setString("language", _lang);

    print(_lang);
  }

  @override
  void initState() {
    getUserData();
    getLang();
    InfoBlock();
    super.initState();
    initOneSignal();
    facebookSignIn.loginBehavior = FacebookLoginBehavior.webViewOnly;
  }

  var isLoading = true;
  var dataInfo;

  void InfoBlock() async {
    final response =
    await http.get("$Url/info.php");
    dataInfo = jsonDecode(response.body);
    print(dataInfo['b']);

    setState(() {
      dataInfo['b'];
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    getUserData();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return _isLoading
        ? Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.white,
      ),
    )
        : Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/fon.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(

            backgroundColor: Colors.transparent,
            elevation: 0.0,
            leading: Text(''),
            flexibleSpace: Container(
              decoration: new BoxDecoration(
                gradient: LinearGradient(
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter,
                    colors: [
                      Color(0xff4267b2),
                      Color(0xff4267b2),
                    ],
                    tileMode: TileMode.repeated),
              ),
            ),

//                iconTheme: IconThemeData(
//                  color: Colors.white, //change your color here
//                ),


            title: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Text("Ваш личный адвокат", style: TextStyle(color: Colors.white)),
                AutoSizeText("Ваш личный адвокат",
                  style: TextStyle(color: Colors.white),
                  maxFontSize: 16,
                  textAlign: TextAlign.center,
                  minFontSize: 9,
                ),
                AutoSizeText("www.personal-lawyer.kz",
                  style: TextStyle(color: Colors.white),
                  maxFontSize: 13,
                  minFontSize: 9,
                ),
              ],
            ),
            //iconTheme: new IconThemeData(color: Colors.black),
          ),
          body: isLoading
              ? Center(
            child: CircularProgressIndicator(),
          )
              : Center(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          //   padding: EdgeInsets.only(top: setTop(), left: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[

//                          Container(
//                            child: Text(
//                              "Ваш личный адвокат",
//                              style: TextStyle(
//                                  fontSize: 22,
//                                  fontWeight: FontWeight.bold,
//                                  color: Colors.white,
//                                  fontFamily: 'MyriadProRegular'),
//                            ),
//                          ),

                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: <Widget>[
                                    /*

                        Container(
                          child: FlatButton(
                            child: Text('Войти', style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: 'MyriadProRegular'),),
                            onPressed: () {
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            }
                          ),
                        ),
                        Icon(Icons.arrow_forward_ios, color: Colors.white,)


                        */
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(
                              //top: MediaQuery.of(context).size.height / 3.8,
                              left: 10.0, right: 10.0,),
                            alignment: Alignment.bottomCenter,
                            decoration: BoxDecoration(
                              //color: Color(0xff4267b2).withOpacity(0.3),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 15, bottom: 15),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                TextField(
                                  onTap: () {
                                    if (_phoneC.text.isEmpty) {
                                      _phoneC.text = "+7";
                                    }
                                  },
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    color: Colors.white,
                                  ),
                                  controller: _phoneC,
                                  keyboardType: TextInputType.number,
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                      hintText: "+7(***)***-**-**",
                                      hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white.withOpacity(
                                                  1))),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xff4267b2)
                                                  .withOpacity(1))),
                                      //                           border: InputBorder.none,
                                      //labelText: _phone,
                                      contentPadding: EdgeInsets.all(7.0),
                                      labelStyle: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w100,
                                        fontSize: 18.0,
                                      )),
                                ),

//                    Container(
//                      height: 50,
//                      decoration: BoxDecoration(
//                        color: Colors.white,
//                        border: Border.all(
//                          color: Colors.black.withOpacity(0.6),
//                        ),
//                        borderRadius: BorderRadius.all(
//                          Radius.circular(5),
//                        ),
//                      ),
//                      child: TextField(
//
//                        enableInteractiveSelection: false,
//                        maxLength: 16,
//                        style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.black.withOpacity(0.9),
//                        ),
//                        controller: _phoneC,
//                        keyboardType: TextInputType.number,
//                        decoration: InputDecoration(
//                          counterText: '',
//                          hintText: "+7(***)***-**-**",
//                          hintStyle: TextStyle(
//                            color: Colors.black.withOpacity(0.3),
//                            fontSize: 18.0,
//                          ),
//                          enabledBorder: UnderlineInputBorder(
//                            borderSide: BorderSide(color: Colors.transparent,),
//                          ),
//                          focusedBorder: UnderlineInputBorder(
//                            borderSide: BorderSide(color: Colors.transparent,),
//                          ),
//                          contentPadding: EdgeInsets.all(10.0),
//                          labelStyle: TextStyle(
//                           color: Colors.white,
//                            fontWeight: FontWeight.w100,
//                            fontSize: 18.0,
//                          ),
//                        ),
//                      ),
//                    ),

                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.black.withOpacity(0.2),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Builder(
                                        builder: (context) =>
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  child: Container(
                                                    height: 50,
                                                    //width: MediaQuery.of(context).size.width,
                                                    child: RaisedButton(
                                                      color: Color(0xff4267b2),
                                                      shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                      ),
                                                      onPressed: () {
                                                        checkUser(context);
                                                      },
                                                      child: Text(
                                                        'Получить SMS-код',
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight: FontWeight
                                                              .bold,
                                                          fontFamily:
                                                          'MyriadProRegular',
                                                          fontSize: 15.0,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                      ),
                                      Container(
                                        padding:
                                        EdgeInsets.only(bottom: 10),
                                        // margin: EdgeInsets.only(bottom: 40),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[

                                            // Builder(
                                            //   builder: (context) =>
                                            //       InkWell(
                                            //         child: Text(
                                            //           'У меня уже есть SMS-код',
                                            //           style: TextStyle(
                                            //               color: Colors.white,
                                            //               fontFamily:
                                            //               'MyriadProRegular',
                                            //               fontWeight: FontWeight
                                            //                   .bold),
                                            //         ),
                                            //         onTap: () {
                                            //           Navigator.pushReplacement(
                                            //               context,
                                            //               MaterialPageRoute(
                                            //                   builder: (
                                            //                       context) =>
                                            //                       MainStartSMSCodeScreen()));
                                            //         },
                                            //       ),
                                            // ),


                                            InkWell(
                                              onTap: () =>
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              ShowContract())),
                                              child: Container(
                                                  padding: EdgeInsets.only(
                                                      top: 15, bottom: 5),
                                                  child: Column(
                                                    //mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      Text(
                                                        'Регистрируясь в мобильном приложении, Вы соглашаетесь с условиями Договора-оферты.',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .white),
                                                        textAlign: TextAlign
                                                            .center,)
                                                    ],
                                                  )),
                                            ),

                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),

          bottomNavigationBar: isLoading
              ? Center(
            child: CircularProgressIndicator(),
          )
              : dataInfo['b'] == 2 ?

          Container(
              height: 50,
              margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 50),
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xff3B5998),
                borderRadius: BorderRadius.circular(5),
              ),
              padding:
              EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
              child: InkWell(
                onTap: () => _loginWithFB(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "assets/facebook.png",
                      width: 70,
                      height: 70,
                    ),
                    Text(
                      'Вход через Facebook',
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ))

              :

          Container(
            child: Text(''),
          ),

          /*

      bottomNavigationBar: Container(
        padding: EdgeInsets.only(bottom: 30,right: 25, left: 25),
        child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: <Widget>[

           InkWell(
             child: Container(
               decoration: new BoxDecoration(
                 color: Color(0xffed5e68),
                 borderRadius: BorderRadius.vertical(
                     top: Radius.circular(10),
                     bottom: Radius.circular(10)
                 ),
               ),
               padding: EdgeInsets.all(10),
               child: Row(
                 children: <Widget>[
                   Container(
                     child: Icon(Icons.chat_bubble, size: 35, color: Colors.white,),
                     padding: EdgeInsets.only(right: 10),
                   ),
                   Text('Заказать услугу', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                 ],
               ),
             ),
             onTap: (){

             },
           ),



           InkWell(
               child: Container(
                 decoration: new BoxDecoration(
                   color: Color(0xff4267b2),
                   borderRadius: BorderRadius.vertical(
                       top: Radius.circular(10),
                       bottom: Radius.circular(10)
                   ),
                 ),
                 padding: EdgeInsets.all(10),
                 child: Row(
                   children: <Widget>[
                     Container(
                       child: Icon(Icons.call, size: 35, color: Colors.white,),
                       padding: EdgeInsets.only(right: 10),
                     ),
                     Text('Позвонить \n8(800)450-95-44   ', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                   ],
                 ),
               ),
             onTap: (){

             },
           ),


         ],

        ),

      )

            */
        ));
  }

  Future checkUser(BuildContext context) async {
    isLoading = true;
   //  Route route =
   //  MaterialPageRoute(builder: (context) => MainStartScreen());
   //  Navigator.pushReplacement(context, route);
   // return;
    if (_phoneC.text.isNotEmpty) {
      var url = "$Url/api/login/send";

      final response = await http.post(url, body: {"phone": _phoneC.text});

      SavePhone();

      print(response.statusCode);
      print(response.body);

      if (response.statusCode == 200) {
        isLoading = false;

        Route route =
        MaterialPageRoute(builder: (context) => MainStartScreen());
        Navigator.pushReplacement(context, route);
      } else {
        Fluttertoast.showToast(
          msg: "Произошла ошибка",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
        );
        throw Exception('Failed to load files');
      }
    } else {
      Fluttertoast.showToast(
        msg: "Введите номер телефона",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  void SavePhone() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneI = _phoneC.text;
    await prefs.setString("phoneI", phoneI);
  }

  Future getUserData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("logged") != null && prefs.getBool("logged")) {
      Route route = MaterialPageRoute(builder: (context) => PagerScreen());
      Navigator.pushReplacement(context, route);
    }
  }

  _loginWithFB() async {
    // _updateLoginInfo();
    _fbLogin();

    //Navigator.push(context, MaterialPageRoute(builder: (context)=>FacebookLogin()));
    // Navigator.push(context, MaterialPageRoute(builder: (context)=>flutterLoginFacebook()));
    // Navigator.push(context, MaterialPageRoute(builder: (context)=>FlutterFbLogin()));
  }

//
//  void _updateLoginInfo() async {
//
//    final plugin = logPlugin;
//    final token = await plugin.accessToken;
//    print('tokenAuth = $token');
//    FacebookUserProfile profile;
//
//
//    if (token != null) {
//      profile = await plugin.getUserProfile();
//      if (token.permissions?.contains(FacebookPermission.email.name) ?? false) {
//       String email = await plugin.getUserEmail();
//       String  name = '${profile.name}  ';
//        authInBackend(  name,   email);
//      }
//    }
//
// }

  void _fbLogin() async {
    _logOut();
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${accessToken
                .token}');
        final profile = jsonDecode(graphResponse.body);
        print('''
         
        Fb token: ${accessToken.token} 
         Profile: ${profile} 
         Permissions: ${accessToken.permissions} 
         ''');
        authInBackend(profile['name'], profile['email']);

        break;
      case FacebookLoginStatus.cancelledByUser:
      //_showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        print('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();
  }

  void authInBackend(String name, String email) async {
    setState(() {
      _isLoading = true;
    });
    var stat = await OneSignal.shared.getPermissionSubscriptionState();
    Map<String, dynamic> params = {
      "name": name,
      "email": email,
      "app_code": stat.subscriptionStatus.userId
    };
    String url = '$Url/api/user-auth/login-with-facebook';
    print('params: $params');

    final response = await http.post(url, body: params);
    print('response login: ${response.body}');

    setState(() {
      _isLoading = false;
    });
    if (response.statusCode == 200) {


      String apikey = json.decode(response.body)['token'];
      String status = json.decode(response.body)['status'];
      print('apikey: ${apikey}');
      print('status: ${status}');

      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString('apikey', apikey);

      // Navigator.push(context,
      //     MaterialPageRoute(builder: (context) => profileEdit()));
      // return;

      if (status == 'registered') {
        Navigator.push(context, MaterialPageRoute(builder: (context) => profileEdit()));
      } else
        loginEnter();
    } else {
      Fluttertoast.showToast(
        msg: "Произошла ошибка",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }
  }

  Future loginEnter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("logged", true);
    Route route = MaterialPageRoute(builder: (context) => PagerScreen());
    Navigator.pushReplacement(context, route);
  }
}
