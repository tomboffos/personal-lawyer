import 'package:flutter/material.dart';
import 'package:personallawyer/orderDocumentPaidCategoryList.dart';
import 'package:personallawyer/orderDocumentPaidPage_step1.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import 'theme_settings.dart';


class orderDocumentPaidCategoryScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentPaidCategoryScreenState();
  }
}


class orderDocumentPaidCategoryScreenState extends State<orderDocumentPaidCategoryScreen>{

  String _api;
  var url;
  var category;
  var categories;
  String _lang;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void getListCategory() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "$Url/api/document/categories/${_lang}/${_api}";

    print(url);

    final response = await http.get(url);

    category = jsonDecode(response.body);

    print(category);

    setState(() {
      category;
      isLoading = false;
    });

    for (int i = 0; i < category.length; i++) {



      print(category[i]['id']);

      if(_lang == 'rus') {



        myList.add(
            new Column(
              children: <Widget>[
                ListTile(
                  title: Text(category[i]['categories']['ru']),
                  trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey,),
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => orderDocumentPaidCategoryListScreen(id: category[i]["id"], title: category[i]['categories']['ru'],)));
                  },
                ),
                Divider(height: 2,),
              ],
            )
        );

      } else {

        myList.add(
            new Column(
              children: <Widget>[
                ListTile(
                  title: Text(category[i]['categories']['kk']),
                  trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey,),
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => orderDocumentPaidCategoryListScreen(id: category[i]["id"], title: category[i]['categories']['kk'])));
                  },
                ),
                Divider(height: 2,),
              ],
            )
        );

      }

    }

  }


  void initState() {
    getListCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  isLoading
          ? Center(
          child: CircularProgressIndicator())
          : SingleChildScrollView(
        child: Container(
            child: Column(
              children: myList,
            ),
        ),
      )

    );
  }

}