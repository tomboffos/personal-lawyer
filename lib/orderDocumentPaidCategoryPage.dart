import 'package:flutter/material.dart';
import 'package:personallawyer/lang.dart';
import 'package:personallawyer/orderDocumentPaidPage_step1.dart';
import 'package:personallawyer/paybox/url_launcher_document.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:share/share.dart';
import 'package:intl/intl.dart';

import 'theme_settings.dart';

class orderDocumentPaidCategoryPageScreen extends StatefulWidget{

  var id; var doc_name; var doc_description; var doc_category; var doc_status; var doc_cost;
  var document_link;

  orderDocumentPaidCategoryPageScreen({Key key, @required this.id, this.doc_name, this.doc_category, this.doc_cost, this.doc_description, this.doc_status, this.document_link}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return orderDocumentPaidCategoryPageScreenState();
  }
}


class orderDocumentPaidCategoryPageScreenState extends State<orderDocumentPaidCategoryPageScreen>{


  String _api;
  var url;
  var data;
  var DocumentData;
  var categories;
  String _lang;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void setInfo() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('DocumentID', data['id'].toString());
    sp.setString('PageDocumentID', widget.id.toString());
    sp.setString('UrlDocumentPay', data['url']);
  }

  void checkDocument() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "$Url/api/document/show/${widget.id}/${_lang}/${_api}";

    final response = await http.get(url);

    print(url);

    DocumentData = jsonDecode(response.body);

    setState(() {
      DocumentData;
      isLoading = false;
    });

  }

  void getLinkPay() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "$Url/api/document/get-payment-url/${widget.id}/${_api}";



    final response = await http.get(url);

    data = jsonDecode(response.body);

    setState(() {
      data['id'];
      setInfo();
      Navigator.push(context, MaterialPageRoute(builder: (context) => LaunchUrlDocument(url: data['url'],)));

    });

  }

  todayDate() {
    DateTime now = new DateTime.now();
    DateTime min = DateTime.parse('1900-01-01 20:00');
    DateTime max = DateTime.parse('1900-01-01 09:00');
    print(now);
    print(min);
    print(max);
    print(now.hour > max.hour && now.hour < min.hour);

    if((now.hour > max.hour && now.hour < min.hour) == false) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[

                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                      child: Text('Ваша заявка будет обработана в рабочее время. Вы готовы ждать?', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600), textAlign: TextAlign.center,),
                    ),

                    Container(
                      padding: EdgeInsets.only(top: 5, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(right: 5),
                              padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                              decoration: BoxDecoration(
                                  color: Color(0xff4267b2),
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Text('Да', style: TextStyle(color: Colors.white),),
                            ),
                            onTap: (){
                              getLinkPay();
                            },
                          ),

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.only(left: 5),
                              padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
                              decoration: BoxDecoration(
                                  color: Color(0xff4267b2),
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: Text('Нет', style: TextStyle(color: Colors.white),),
                            ),
                            onTap: (){
                              Navigator.pop(context);
                            },
                          )

                        ],
                      ),
                    )

                  ],
                ),
              )
            ]);
          });
    } else {
      getLinkPay();
    }



 //   getLinkPay();


  }


  void initState() {
    checkDocument();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Документ №: ' + widget.id.toString()),
          flexibleSpace: Container(
            decoration: new BoxDecoration(
              gradient: LinearGradient(
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  colors: [
                    Color(0xff4267b2),
                    Color(0xff4267b2),
                  ],
                  tileMode: TileMode.repeated),
            ),
          ),
        ),
        body:    isLoading
            ? Center(
            child: CircularProgressIndicator())
            : SingleChildScrollView(
            child: Container (
              padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
              child: Column(
                children: <Widget>[

                  Container(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(widget.doc_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                        ),

                        Container(
                          padding: EdgeInsets.only(top: 15),
                          child: Row(
                            children: <Widget>[
                              Text('Тип документ:', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              Container(
                                padding: EdgeInsets.only(left: 5),
                                child: Text(widget.doc_status, style: TextStyle(fontSize: 14),),
                              )
                            ],
                          ),
                        ),

                        Container(
                          padding: EdgeInsets.only(top: 5),
                          child: Row(
                            children: <Widget>[
                              Text('Цена:', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              Container(
                                padding: EdgeInsets.only(left: 5),
                                child: Text(widget.doc_cost + ' тенге', style: TextStyle(fontSize: 14),),
                              )
                            ],
                          ),
                        ),

                        widget.doc_description == 'null' || widget.doc_description == null || widget.doc_description == '' ? Container() :

                        Container(
                          padding: EdgeInsets.only(top: 15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Описание:', style: TextStyle(color: Colors.grey, fontSize: 14),),
                              Container(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(widget.doc_description, style: TextStyle(fontSize: 14),),
                              )
                            ],
                          ),
                        ),

                        DocumentData['data']['hasPaid'] == 0 ?

                        Container()

                            :

                        Container(
                          padding: EdgeInsets.only(top: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              InkWell(
                                child: Container(
                                  margin: EdgeInsets.only(top: 10),
                                  width: MediaQuery.of(context).size.width / 2.3,
                                  padding: EdgeInsets.all(10) ,
                                  decoration: BoxDecoration(
                                    color: Color(0xff72b923),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(trans('download_document', _lang), textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                                ),
                                onTap: (){
                                  UrlLauncher.launch(DocumentData['data']['document_link']);
                                },
                              ),

                              InkWell(
                                child: Container(
                                  margin: EdgeInsets.only(top: 10),
                                  width: MediaQuery.of(context).size.width / 2.3,
                                  padding: EdgeInsets.all(10) ,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(trans('share_this', _lang), textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                                ),
                                onTap: (){
                                  Share.share(DocumentData['data']['document_link']);
                                },
                              )

                            ],
                          ),
                        )


                      ],
                    ),
                  ),




                ],
              ),
            )
        ),

      bottomNavigationBar:   isLoading
          ? Center(
          child: CircularProgressIndicator())
          : Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        child: DocumentData['data']['hasPaid'] == 1 ?

            Container()

//        Container(
//          color: Colors.grey,
//          child: FlatButton(
//
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  Container(
//                    padding: EdgeInsets.only(left: 15),
//                    child: Text('Вы уже заказывали этот документ.',
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 15
//                        )
//                    ),
//                  ),
//
//
//                ],
//              )
//          ),
//        )
            :
            Container(
              color: Colors.green,
              child: FlatButton(
                  onPressed: () {
                    todayDate();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.payment,
                          color: Colors.white
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 15),
                        child: Text('Оплатить',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15
                            )
                        ),
                      ),
                    ],
                  )
              ),
            )
      ),

    );
  }

}