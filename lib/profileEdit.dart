import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/AskQuestion_step1.dart';
import 'package:personallawyer/AskQuestion_step2City.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:personallawyer/orderDocumentPaidPage_step2City.dart';
import 'package:personallawyer/orderDocumentPaidPage_step3.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'AskQuestion_step3.dart';
import 'PageView.dart';
import 'lang.dart';
import 'profileSelectCity.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:http/http.dart' as http;

import 'theme_settings.dart';

class profileEdit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return profileEditState();
  }
}

class profileEditState extends State<profileEdit> {
  var data;
  String phoneUser;
  String nameTextInput;
  String emailTextInput;
  var user;
  String _lang;

  TextEditingController nameText = new TextEditingController();
  TextEditingController emailText = new TextEditingController();
  TextEditingController cityText = new TextEditingController();
  TextEditingController phoneText = new TextEditingController();

  String textCity='';
  int idCity;

  TextEditingController maskedText =
      new MaskedTextController(mask: '+7(000)000-00-00');

  bool _isLoading = false;

  void getUserData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    textCity = sp.getString("cityText");
    phoneUser = sp.getString("phone_number_user");
    nameTextInput = sp.getString("nameText");
    emailTextInput = sp.getString("emailText");

    nameText.text = nameTextInput;
    emailText.text = emailTextInput;
    maskedText.text = phoneUser;

    setState(() {
      textCity;
      phoneUser;
      nameTextInput;
      emailTextInput;
      emailText;
      nameText;
    });
  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    super.initState();
    setUser().then((value) {
      getUserData();
    });

    getLang();
  }

  Future<void> setUser() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String _api = sp.getString("apikey");

    print(_api);

    String url =
        "$Url/api/$_api/about_you/user/user_data";

    final response = await http.get(url);

    user = jsonDecode(response.body);

    print('user $user');

    sp.setString('user_image', user["image_link"]);
    sp.setString('phone_number_user', user["phone_number"]);
    sp.setString('nameText', user['user_name']);
    sp.setString('emailText', user['user_email']);
    sp.setString('cityID', user['user_city']);
  }

  openCityList() async {
    Map<String, dynamic> params = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => profileSelectCity()));
    if (params != null) {
      textCity = params["cityText"];
      idCity = params["cityID"];
    }
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        leading:Container(),
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text('Изменение профиля', style: TextStyle(color: Colors.white)),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Container(
              padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(top: 5, bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text(
                            trans('full_name', _lang),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )),
                  Container(
                    child: TextField(
                      controller: nameText,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: trans('field_to_fill', _lang),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff4267b2), width: 0.75)),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text) {},
                    ),
                  ),

                  Container(
                      padding: EdgeInsets.only(top: 15, bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text(
                            "E-mail:",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )),
                  Container(
                    child: TextField(
                      controller: emailText,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: trans('field_to_fill', _lang),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff4267b2), width: 0.75)),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text) {},
                    ),
                  ),

                  Container(
                      padding: EdgeInsets.only(top: 25, bottom: 10),
                      child: InkWell(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            textCity==null||textCity.length==0
                                ? Text(
                                    trans('choose_a_city', _lang),
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.bold),
                                  )
                                : Container(child: Text(textCity)),
                            Icon(Icons.keyboard_arrow_right, color: Colors.grey)
                          ],
                        ),
                        onTap: () {
                          openCityList();
                        },
                      )),

                  Divider(
                    height: 2,
                  ),

//                  Container(
//                      padding: EdgeInsets.only(top:15, bottom:10),
//                      width: MediaQuery.of(context).size.width,
//                      child: Row(
//                        children: <Widget>[
//                          Text("Город:",
//                            textAlign: TextAlign.start,
//                            style: TextStyle(color: Colors.grey,
//                                fontWeight: FontWeight.bold
//                            ),
//                          ),
//                        ],
//                      )
//                  ),
//                  Container(
//                    child: TextField(
//                      //  controller: _nameC,
//                      textCapitalization: TextCapitalization.words,
//                      textInputAction: TextInputAction.done,
//                      decoration: InputDecoration(
//                        hintText: '',
//                        hintStyle: TextStyle(color: Colors.grey[200]),
//                        filled: true,
//                        fillColor: Colors.white,
//                        enabledBorder: OutlineInputBorder(
//                          borderSide: BorderSide(color: Colors.grey[300]),
//                        ),
//                        focusedBorder: OutlineInputBorder(
//                            borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
//                        ),
//                        //labelText: _title,
//                        contentPadding: EdgeInsets.all(12.0),
//                      ),
//                      onChanged: (String text){
//
//                      },
//                    ),
//                  ),

                  Container(
                      padding: EdgeInsets.only(top: 15, bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text(
                            trans('phone', _lang),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )),
                  Container(
                    child: TextField(
                      controller: maskedText,

                      textCapitalization: TextCapitalization.sentences,
                      //textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        //   hintText: '+7(000)000-00-00',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xff4267b2), width: 0.75)),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      bottomNavigationBar: Container(
        height: 60,
        child: FlatButton(
            color: Color(0xff3B5998),
            onPressed: () {
              sendRequest();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.send, color: Colors.white),
                Text("  Сохранить", style: TextStyle(color: Colors.white))
              ],
            )),
      ),
    );
  }

  sendRequest() async {
    if (textCity.length > 0 && (maskedText.text.length > 0)) {
      setState(() {
        _isLoading = true;
      });
      SharedPreferences sp = await SharedPreferences.getInstance();
      var _api = sp.getString("apikey");
      sp.setString('cityText', textCity);
      sp.setString('cityID', idCity.toString());

      Map<String, dynamic> params = {
        "phone": maskedText.text,
        "token": _api,
        "city": idCity.toString()
      };
      String url = '$Url/api/user-auth/profile/add';
      print('params: $params');
      print(url);

      final response = await http.post(url, body: params);
      print('response login: ${response.body}');
      setState(() {
        _isLoading = false;
      });
      if (response.statusCode == 200) {
        loginEnter();
      } else {
        Fluttertoast.showToast(
            msg: jsonDecode(response.body)['error'],
            toastLength: Toast.LENGTH_SHORT,
            timeInSecForIos: 1);
      }
    } else
      Fluttertoast.showToast(
          msg: trans('please_fill_in_the_fields', _lang),
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIos: 1);
  }

  Future loginEnter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("logged", true);
    Route route = MaterialPageRoute(builder: (context) => PagerScreen());
    Navigator.pushReplacement(context, route);
  }
}
