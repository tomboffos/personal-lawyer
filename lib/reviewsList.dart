import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:personallawyer/theme_settings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:share/share.dart';
import 'lang.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';


class reviewsList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return reviewsListState();
  }
}

class reviewsListState extends State<reviewsList> {

  String _lang;
  String _api;
  var url;
  var data;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }


  void reviewsList() async {


    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "$Url/api/review-list/$_lang/$_api/";

    final response = await http.get(url);

   print(url);

    data = jsonDecode(response.body);


    setState(() {
      data;
      isLoading = false;
    });





    for (int i = 0; i < data.length; i++) {
        myList.add(
            new Column(
              children: <Widget>[
                Container(
                  // color: Colors.blue[50],
                  //  padding: EdgeInsets.only(top: 15, left: 20, right: 20, bottom: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        Container(
                            padding: EdgeInsets.only(top: 15, right: 20, left: 20),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[

                                //   Text('Дата: ', style: TextStyle(fontWeight: FontWeight.w600),),

                                Text(data[i]['created'], style: TextStyle(fontSize: 13),)

                              ],
                            )
                        ),

                        Container(
                          padding: EdgeInsets.only(top: 10, left: 20),
                          child: Text(data[i]['reviews']),
                        ),

                        Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            border: Border(
                              top: BorderSide(width: 0.3, color: Colors.grey),
                              bottom: BorderSide(width: 0.3, color: Colors.grey),
                            ),
                          ),
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[

                              Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(trans('author', _lang), style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13),),
                                    Container(
                                      padding: EdgeInsets.only(top: 2.5),
                                      child: data[i]['author']['name'] == null ? Text('Безымянный') : Text(data[i]['author']['name'], style: TextStyle(fontSize: 13),),
                                    )
                                  ],
                                ),
                              ),

                              Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(trans('lawyer', _lang), style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13),),
                                    Container(
                                      padding: EdgeInsets.only(top: 2.5),
                                      child: Text(data[i]['user']['name'], style: TextStyle(fontSize: 13),),
                                    )
                                  ],
                                ),
                              ),

                            ],
                          ),
                        )


                      ],
                    )
                ),
              ],
            )
        );


    }



  }

  void initState() {
    getLang();
    reviewsList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('reviews4', _lang), style: TextStyle(color: Colors.white)),
      ),

      body:   isLoading
          ? Center(
          child: CircularProgressIndicator())
          : SingleChildScrollView(
        child: Container(
          child: Column(
            children: myList,
          ),
        ),
      )
    );
  }
}