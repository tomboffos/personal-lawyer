import 'dart:io';

import 'package:flutter/material.dart';

import 'theme_settings.dart';

import 'package:share/share.dart';

Widget buildInit() {
  return Center(
    child: CircularProgressIndicator(
      backgroundColor: Colors.transparent,
      valueColor: new AlwaysStoppedAnimation<Color>(Colors69),
    ),
  );
}

Widget buildEmpty(String no_data) {
  return Center(child: Text(no_data));
}

shareApp() {
  if (Platform.isAndroid) {
    Share.share(
        "Мобильное приложение Personal lawyer (Ваш личный адвокат) предназначено для организации и проведения юридических консультаций, а также для информационно-аналитического обеспечения клиентов по юридическим вопросам. Скачать: https://play.google.com/store/apps/details?id=kz.itgroup.personallawyer&hl=ru");
  } else if (Platform.isIOS) {
    Share.share(
        "Мобильное приложение Personal lawyer (Ваш личный адвокат) предназначено для организации и проведения юридических консультаций, а также для информационно-аналитического обеспечения клиентов по юридическим вопросам. Скачать: https://apps.apple.com/us/app/id1499337941");
  }
}
