import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class editProfileScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return editProfileScreenState();
  }
}

class editProfileScreenState extends State<editProfileScreen> {

var data;

//  void AddReviews() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    var _api = prefs.getString("apikey");
//
//    if(textDesc.text.isNotEmpty){
//      var response = await http.post("https://advokat.vipsite.kz/api/profile/reviews_add",
//          body:{
//            'api_token': _api,
//            'reviews': textDesc.text,
//          });
//
//      if(response.statusCode == 200){
//
//        data = jsonDecode(response.body);
//
//        Fluttertoast.showToast(
//          msg: data,
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.BOTTOM,
//          timeInSecForIos: 5,
//          backgroundColor: Colors.green,
//          textColor: Colors.white,
//        );
//
//      }
//
//
//    } else {
//      Fluttertoast.showToast(
//        msg: "Заполните все обязательные поля!",
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.BOTTOM,
//        timeInSecForIos: 1,
//        backgroundColor: Colors.red,
//        textColor: Colors.white,
//      );
//    }
//
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text("Изменение профиля", style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
              child: Column(
                children: <Widget>[


                  Container(
                      padding: EdgeInsets.only(top:5, bottom:10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text("ФИО:",
                            textAlign: TextAlign.start,
                            style: TextStyle(color: Colors.grey,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    child: TextField(
                    //  controller: _nameC,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: 'Муратов Тимур Нурланович',
                        hintStyle: TextStyle(color: Colors.grey[200]),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue, width: 0.75)
                        ),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text){

                      },
                    ),
                  ),

                  Container(
                      padding: EdgeInsets.only(top:5, bottom:10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text("ФИО:",
                            textAlign: TextAlign.start,
                            style: TextStyle(color: Colors.grey,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    child: TextField(
                      //  controller: _nameC,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: 'Муратов Тимур Нурланович',
                        hintStyle: TextStyle(color: Colors.grey[200]),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue, width: 0.75)
                        ),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text){

                      },
                    ),
                  ),




                ],
              ),
            )
        )
    );
  }
}